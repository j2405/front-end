import chroma from "chroma-js";

const purpleColor = chroma("#662bb7e6");

export const theme = {
  navBackgroundColor: "#9a76cb",
  navButtonsColor: "",

  buttonsColor: "#fff",
  buttonsAddSubColor: purpleColor.hex(),
  buttonsBorderColor: "#662bb7d4",
  buttonsBackgroundColor: "#662bb7d4",

  buttonsHoverBackgroundColor: purpleColor.hex(),
  buttonsHoverColor: "#fff",
  buttonsBorderHoverColor: purpleColor.hex(),

  iconColor: "#662bb7",
  iconHoverColor: "",

  footerIconColor: "#9a76cb",
  footerIconHoverColor: purpleColor.hex(),

  footerBackgroundColor: "#f9fafb",

  rangeColor: "#630dd9",

  selectActiveBorderColor: purpleColor.hex(),
  selectOptionHoverColor: purpleColor.alpha(0.4).css(),

  homeNewArrivalsTabBorderColor: purpleColor.hex(),
  homeNewArrivalsTabColor: purpleColor.alpha(0.8).css(),
};

export const reactSelectTheme = (t) => ({
  ...t,
  colors: {
    ...t.colors,
    primary25: theme.selectOptionHoverColor,
    primary: theme.selectActiveBorderColor,
  },
});
