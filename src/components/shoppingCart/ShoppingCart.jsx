import { useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import { getCartProductInfo } from "../../apis/products";
import { fadeInAnimation } from "../../constants";
import {
  NormalButton,
  StyledHeading,
} from "../commonComponents/CommonStyledComponents";
import { useCartContext } from "../contexts/CartContext";
import CartProduct from "./CartProduct";

const ShoppingCart = () => {
  const [cartProductsInfo, setCartProductsInfo] = useState(null);
  const { cartItems, setCart } = useCartContext();
  const navigate = useNavigate();

  useEffect(() => {
    getCartProductInfo(cartItems).then((response) =>
      setCartProductsInfo(response.data)
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const deleteProductCard = useCallback(
    (id) =>
      setCartProductsInfo((prevState) => ({
        ...prevState,
        products: prevState.products.filter((product) => product.id !== id),
      })),
    []
  );

  const changeProductCount = useCallback(
    (id, newCount) =>
      setCartProductsInfo((prevState) => {
        const newProducts = prevState.products;
        let productToChange = newProducts.find((product) => product.id === id);
        productToChange.count = newCount;
        return {
          ...prevState,
          products: newProducts,
        };
      }),
    []
  );

  const onProceedCheckout = useCallback(() => {
    const items = cartProductsInfo.products.reduce((previousValue, product) => {
      const newProducts = [...previousValue];
      for (let index = 0; index < product.count; index++) {
        newProducts.push(product.id);
      }

      return newProducts;
    }, []);

    setCart(items);
    navigate("/checkout");
  }, [navigate, setCart, cartProductsInfo]);

  return (
    <section className="h-100">
      <div className="container h-100 py-2">
        <div className="row d-flex justify-content-center align-items-center h-100">
          <div className="col-12">
            <div className="d-flex justify-content-between align-items-center mb-4">
              <StyledHeading className="fw-normal mb-0 text-black text-center w-100">
                Shopping Cart
              </StyledHeading>
            </div>

            {cartProductsInfo &&
              cartProductsInfo.products.map((product) => (
                <CartProduct
                  {...product}
                  key={product.id}
                  deleteProductCard={deleteProductCard}
                  changeProductCount={changeProductCount}
                />
              ))}

            {cartProductsInfo && cartProductsInfo.products.length === 0 && (
              <h4 className="text-center">No items in the cart</h4>
            )}

            {/* <div className="card mb-4">
            <div className="card-body p-4 d-flex flex-row">
              <div className="form-outline flex-fill">
                <input
                  type="text"
                  id="form1"
                  className="form-control form-control-lg"
                />
                <label className="form-label" for="form1">
                  Discound code
                </label>
              </div>
              <button
                type="button"
                className="btn btn-outline-warning btn-lg ms-3"
              >
                Apply
              </button>
            </div>
          </div> */}
            {cartProductsInfo && cartProductsInfo.products.length > 0 && (
              <PaymentContainer className="card">
                <div className="card-body">
                  <NormalButton
                    type="button"
                    className="btn-block btn-lg w-100"
                    onClick={onProceedCheckout}
                  >
                    Proceed to Payment
                  </NormalButton>
                </div>
              </PaymentContainer>
            )}
          </div>
        </div>
      </div>
    </section>
  );
};

export default ShoppingCart;

const PaymentContainer = styled.div`
  animation: 3s ${fadeInAnimation};
`;
