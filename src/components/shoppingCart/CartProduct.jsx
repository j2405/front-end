import { Button, Form, Image } from "react-bootstrap";
import styled from "styled-components";
import { slideAnimation } from "../../constants";
import { theme } from "../../theme";

const CartProduct = ({
  productName,
  price,
  count,
  deleteProductCard,
  id,
  changeProductCount,
  imageUrls,
}) => {
  return (
    <ProductContainer className="card rounded-3 mb-4">
      <div className="card-body p-4">
        <div className="row d-flex justify-content-between align-items-center">
          <div className="col-md-2 col-lg-2 col-xl-2">
            <ProductImage
              src={imageUrls[0] ?? "./images/ring.png"}
              className="img-fluid rounded-3"
              alt="Cotton T-shirt"
            />
          </div>
          <div className="col-md-3 col-lg-3 col-xl-3">
            <p className="lead fw-normal mb-2">{productName}</p>
          </div>
          <div className="col-md-3 col-lg-3 col-xl-2 d-flex">
            <Button
              variant="link"
              onClick={() => changeProductCount(id, count - 1)}
              disabled={count === 1}
            >
              <ColoredIcon className="fa fa-minus"></ColoredIcon>
            </Button>

            <QuantityInput
              id="form1"
              name="quantity"
              value={count}
              onChange={() => null}
              className="form-control form-control-sm mx-2"
            />

            <Button
              variant="link"
              onClick={() => changeProductCount(id, count + 1)}
            >
              <ColoredIcon className="fa fa-plus"></ColoredIcon>
            </Button>
          </div>
          <div className="col-md-3 col-lg-2 col-xl-2 offset-lg-1">
            <h5 className="mb-0">{price} лв</h5>
          </div>
          <div className="col-md-1 col-lg-1 col-xl-1 text-end">
            <Button variant="link" onClick={() => deleteProductCard(id)}>
              <i className="fa fa-trash fa-lg text-danger"></i>
            </Button>
          </div>
        </div>
      </div>
    </ProductContainer>
  );
};

export default CartProduct;

const QuantityInput = styled(Form.Control)`
  text-align: center;
  pointer-events: none;
`;

const ProductContainer = styled.div`
  animation: 2s ${slideAnimation};
  :hover {
    background-color: #f5f5f5;
  }
`;

const ProductImage = styled(Image)`
  height: 8rem;
  width: 8rem;
`;

const ColoredIcon = styled.i`
  color: ${theme.buttonsAddSubColor};
`;
