import { Container } from "react-bootstrap";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import ContactUs from "./contactUs/ContactUs";
import Home from "./home/Home";
import Login from "./login/Login";
import Register from "./login/Register";
import Navigation from "./navigation/Navigation";
import { Toaster } from "react-hot-toast";
import CustomOrder from "./customOrder/CustomOrder";
import CreateMaterialForm from "./admin/CreateMaterialForm";
import CreateCategoryForm from "./admin/CreateCategoryForm";
import CreateProductForm from "./admin/CreateProductForm";
import styled from "styled-components";
import ProductsPage from "./products/ProductsPage";
import CheckoutForm from "./checkout/CheckoutForm";
import { ReactSession } from "react-client-session";
import { CartProvider } from "./contexts/CartContext";
import Footer from "./footer/Footer";
import SuccessfulOrder from "./checkout/SuccessfullOrder";
import ShoppingCart from "./shoppingCart/ShoppingCart";
import OrdersList from "./orders/OrdersList";
import axios from "axios";
import CreateGemForm from "./admin/CreateGemForm";

ReactSession.setStoreType("localStorage");

axios.interceptors.request.use(function (config) {
  const token = ReactSession.get("token");
  config.headers.Authorization = token;

  return config;
});

const App = () => (
  <CartProvider>
    <BrowserRouter>
      <Navigation />
      <NavigationMargined />
      <Container>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/contact" element={<ContactUs />} />
          <Route exact path="/custom" element={<CustomOrder />} />
          <Route exact path="/login" element={<Login />} />
          <Route exact path="/register" element={<Register />} />
          <Route exact path="/products" element={<ProductsPage />} />
          <Route exact path="/cart" element={<ShoppingCart />} />
          <Route exact path="/checkout" element={<CheckoutForm />} />
          <Route exact path="/checkout/success" element={<SuccessfulOrder />} />
          <Route exact path="/previous-orders" element={<OrdersList />} />
          <Route
            exact
            path="/material/create"
            element={<CreateMaterialForm />}
          />
          <Route exact path="/gem/create" element={<CreateGemForm />} />
          <Route
            exact
            path="/category/create"
            element={<CreateCategoryForm />}
          />
          <Route exact path="/product/create" element={<CreateProductForm />} />
        </Routes>
      </Container>

      <Toaster
        toastOptions={{
          duration: 5000,
        }}
      />

      <FooterMargined />
      <Footer />
    </BrowserRouter>
  </CartProvider>
);

export default App;

const NavigationMargined = styled.div`
  margin-top: 3.5rem;
`;

const FooterMargined = styled.div`
  margin-top: 7rem;
`;
