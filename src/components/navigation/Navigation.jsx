import { useEffect, useState } from "react";
import {
  Col,
  Container,
  Dropdown,
  DropdownButton,
  Nav,
  Navbar,
  Row,
} from "react-bootstrap";
import { Link, useNavigate, createSearchParams } from "react-router-dom";
import styled from "styled-components";
import { getAllCategories } from "../../apis/categories";
import {
  HooverableIcon,
  HooverableLink,
} from "../commonComponents/CommonStyledComponents";
import { useCartContext } from "../contexts/CartContext";
import { slideAnimation } from "../../constants";
import { theme } from "../../theme";

const Navigation = () => {
  const [displayCategories, setDisplayCategories] = useState(false);
  const [displaySearch, setDisplaySearch] = useState(false);
  const [categories, setCategories] = useState([]);
  const [searchName, setSearchName] = useState("");

  const { cartItems } = useCartContext();

  const navigate = useNavigate();

  useEffect(() => {
    getAllCategories().then((response) => setCategories(response.data));
  }, []);

  const isAdmin = true;
  const isLoggedIn = true;
  return (
    <div
      className="fixed-top"
      onMouseLeave={(e) => {
        setDisplayCategories(false);
      }}
    >
      <StyledNav variant="dark">
        <Container>
          <Nav className="me-auto">
            <Nav.Link
              as={Link}
              to="/"
              onMouseEnter={(e) => {
                setDisplayCategories(false);
              }}
            >
              Home
            </Nav.Link>
            <Nav.Link
              onMouseEnter={(e) => {
                setDisplayCategories(true);
              }}
            >
              Categories
            </Nav.Link>
            <Nav.Link
              as={Link}
              to="/products"
              onMouseEnter={(e) => {
                setDisplayCategories(false);
              }}
            >
              Products
            </Nav.Link>
            {isLoggedIn && (
              <Nav.Link
                as={Link}
                to="/custom"
                onMouseEnter={(e) => {
                  setDisplayCategories(false);
                }}
              >
                Custom Order
              </Nav.Link>
            )}
            <Nav.Link
              as={Link}
              to="/contact"
              onMouseEnter={(e) => {
                setDisplayCategories(false);
              }}
            >
              Contact Us
            </Nav.Link>
          </Nav>
          <div
            onMouseEnter={(e) => {
              setDisplayCategories(false);
            }}
          >
            <SearchNavButton
              type="button"
              className="btn btn-link"
              onClick={() => setDisplaySearch(!displaySearch)}
            >
              <HooverableIcon className="fa fa-search" aria-hidden="true" />
            </SearchNavButton>
            <UserDropdown
              id="dropdown-basic-button"
              variant="link"
              className="d-inline"
              title={
                <HooverableIcon className="fa fa-user" aria-hidden="true" />
              }
              onMouseEnter={(e) => {
                setDisplayCategories(false);
              }}
            >
              {isLoggedIn && (
                <Dropdown.Item as={Link} to="/previous-orders">
                  Previous orders
                </Dropdown.Item>
              )}

              {isAdmin && (
                <>
                  <Dropdown.Item as={Link} to="/product/create">
                    Create Product
                  </Dropdown.Item>
                  <Dropdown.Item as={Link} to="/material/create">
                    Create Material
                  </Dropdown.Item>
                  <Dropdown.Item as={Link} to="/category/create">
                    Create Category
                  </Dropdown.Item>
                  <Dropdown.Item as={Link} to="/gem/create">
                    Create Gem
                  </Dropdown.Item>
                </>
              )}
            </UserDropdown>

            <CartButton
              type="button"
              className="btn btn-link"
              onClick={() => navigate("/cart")}
            >
              <HooverableIcon
                className="fa fa-shopping-cart"
                aria-hidden="true"
              />
              {cartItems.length !== 0 && (
                <ItemsCount>{cartItems.length}</ItemsCount>
              )}
            </CartButton>
          </div>
        </Container>
      </StyledNav>
      {displayCategories && (
        <CategoriesContainer className="pt-3">
          <Row>
            {categories.map((category) => (
              <Col key={category.id} md="3" className="mb-3">
                <HooverableLink to={`/products?categoryId=${category.id}`}>
                  <b>{category.name}</b>
                </HooverableLink>
              </Col>
            ))}
          </Row>
        </CategoriesContainer>
      )}

      {!displayCategories && displaySearch && (
        <CategoriesContainer className="pt-3 pb-1">
          <form
            onSubmit={(e) => {
              e.preventDefault();
              navigate({
                pathname: "/products",
                search: `?${createSearchParams({ name: searchName })}`,
              });
              setDisplaySearch(false);
            }}
          >
            <SearchInput
              type="search"
              className="form-control"
              placeholder="Search..."
              aria-label="Search"
              value={searchName}
              onChange={(e) => setSearchName(e.target.value)}
            />
            <SearchButton
              type="button"
              className="btn btn-link"
              onClick={() => {
                navigate({
                  pathname: "/products",
                  search: `?${createSearchParams({ name: searchName })}`,
                });
                setDisplaySearch(false);
              }}
            >
              <HooverableIcon className="fa fa-search" aria-hidden="true" />
            </SearchButton>
          </form>
        </CategoriesContainer>
      )}
    </div>
  );
};

export default Navigation;

const StyledNav = styled(Navbar)`
  background-color: #9a76cb;
`;

const CategoriesContainer = styled(Container)`
  position: relative;
  background: white;
  animation: 1s ${slideAnimation};
`;

const SearchButton = styled.button`
  position: absolute;
  top: 0.75rem;
  bottom: 0;
  padding: 0;
  width: 2.25rem;
  left: 0.75rem;
`;

const SearchInput = styled.input`
  padding: 0 2.25rem;
  height: 2.25rem;
`;

const CartButton = styled.button`
  position: relative;
  &:hover i {
    color: #b2c2d0;
  }
`;

const SearchNavButton = styled.button`
  &:hover i {
    color: #b2c2d0;
  }
`;

const ItemsCount = styled.span`
  color: red;
  position: absolute;
  border-radius: 50%;
  border: none;
  font-size: 0.8rem;
  bottom: 0;
  right: 0.4rem;
  font-weight: bold;
`;

const UserDropdown = styled(DropdownButton)`
  button {
    color: #1a1f36;
  }

  .dropdown-item:hover {
    background-color: ${theme.selectOptionHoverColor};
  }
`;
