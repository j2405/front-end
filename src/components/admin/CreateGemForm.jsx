import { useCallback, useState } from "react";
import { FloatingLabel, Form, Row } from "react-bootstrap";
import toast from "react-hot-toast";
import { createGem } from "../../apis/gems";
import {
  ResponsiveButton,
  StyledHeading,
} from "../commonComponents/CommonStyledComponents";

const validateForm = (formValues) => {
  const errList = [];
  if (!formValues.name) {
    errList.push("Name is required!");
  }

  return errList;
};

const CreateGemForm = () => {
  const [gemCreationData, setGemCreationData] = useState({
    name: "",
  });

  const clearForm = () => {
    setGemCreationData({
      name: "",
    });
  };

  const onSubmit = useCallback(() => {
    const errors = validateForm(gemCreationData);
    if (errors.length > 0) {
      toast.error(errors.join("\n"));
      return;
    }

    toast.promise(createGem(gemCreationData), {
      loading: "Loading...",
      success: () => {
        clearForm();
        return "Successfully created!";
      },
      error: "Error while sending!",
    });
  }, [gemCreationData]);

  const handleOnChange = (e) => {
    toast.dismiss();
    setGemCreationData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  return (
    <section className="mb-4">
      <StyledHeading className="h1-responsive font-weight-bold text-center my-4">
        Create Gem
      </StyledHeading>

      <Row>
        <div className="col-md-12 mb-md-0 mb-5">
          <form
            id="contact-form"
            name="contact-form"
            onSubmit={(e) => {
              e.preventDefault();
              onSubmit();
            }}
          >
            <Row className="mb-2 p-3 justify-content-center">
              <div className="col-md-6 mb-5 mb-md-0">
                <div className="md-form mb-0">
                  <FloatingLabel label="Gem name">
                    <Form.Control
                      type="text"
                      onChange={handleOnChange}
                      name="name"
                      placeholder="Gem name"
                      value={gemCreationData.name}
                    />
                  </FloatingLabel>
                </div>
              </div>
            </Row>
          </form>

          <div className="text-center text-md-left p-3">
            <ResponsiveButton
              className="btn btn-primary"
              onClick={onSubmit}
              tabletWidth={30}
            >
              Create
            </ResponsiveButton>
          </div>
          <div className="status"></div>
        </div>
      </Row>
    </section>
  );
};

export default CreateGemForm;
