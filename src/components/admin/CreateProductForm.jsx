import { useCallback, useEffect, useMemo, useState } from "react";
import { FloatingLabel, Form, Row } from "react-bootstrap";
import toast from "react-hot-toast";
import Select from "react-select";
import styled from "styled-components";
import { getAllCategories } from "../../apis/categories";
import { getAllGems } from "../../apis/gems";
import { getAllMaterials } from "../../apis/materials";
import { createProducts } from "../../apis/products";
import { reactSelectTheme } from "../../theme";
import {
  ResponsiveButton,
  StyledHeading,
} from "../commonComponents/CommonStyledComponents";
import FileUpload from "../commonComponents/FileUpload";

const validateForm = (formValues, files) => {
  const errList = [];
  if (!formValues.name) {
    errList.push("Name is required!");
  }

  if (!formValues.categoryId) {
    errList.push("Category is required!");
  }

  if (!formValues.price) {
    errList.push("Price is required!");
  }

  if (!formValues.quantity) {
    errList.push("Quantity is required!");
  }

  if (formValues.materials.length === 0) {
    errList.push("Please select some materials!");
  }

  if (formValues.gems.length === 0) {
    errList.push("Please select a gem!");
  }

  if (files.length === 0) {
    errList.push("Please upload picture of the jewelry!");
  }

  return errList;
};

const CreateProductForm = () => {
  const [files, setFiles] = useState([]);
  const [productCreationData, setProductCreationData] = useState({
    name: "",
    categoryId: "",
    quantity: 0,
    price: 0.0,
    materials: [],
    gems: [],
  });

  const [categories, setCategories] = useState([]);
  const [materials, setMaterials] = useState([]);
  const [gems, setGems] = useState([]);

  useEffect(() => {
    getAllCategories().then((response) =>
      setCategories(
        response.data.map((cat) => ({ value: cat.id, label: cat.name }))
      )
    );
  }, [setCategories]);

  useEffect(() => {
    getAllMaterials().then((response) => setMaterials(response.data));
  }, [setMaterials]);

  useEffect(() => {
    getAllGems().then((response) => setGems(response.data));
  }, [setGems]);

  const onSubmit = useCallback(() => {
    const errors = validateForm(productCreationData, files);
    if (errors.length > 0) {
      toast.error(errors.join("\n"));
      return;
    }

    toast.promise(
      createProducts({
        ...productCreationData,
        files: files.map((file) => file.file),
      }),
      {
        loading: "Loading...",
        success: () => {
          clearForm();
          return "Successfully created!";
        },
        error: "Error while sending!",
      }
    );
  }, [productCreationData, files]);

  const handleOnChange = (e) => {
    toast.dismiss();
    setProductCreationData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const clearForm = () => {
    setProductCreationData({
      name: "",
      categoryId: "",
      quantity: 0,
      price: 0.0,
      materials: [],
      gems: [],
    });
    setFiles([]);
  };

  const onMaterialsSelect = (selectedMaterials) => {
    toast.dismiss();
    setProductCreationData((prevState) => ({
      ...prevState,
      materials: selectedMaterials.map((material) => material.value),
    }));
  };

  const onGemsSelect = (selectedGems) => {
    toast.dismiss();
    setProductCreationData((prevState) => ({
      ...prevState,
      gems: selectedGems.map((gem) => gem.value),
    }));
  };

  const onCategorySelect = (newCategory) => {
    toast.dismiss();
    setProductCreationData((prevState) => ({
      ...prevState,
      categoryId: newCategory.value,
    }));
  };

  const updateFiles = (incomingFiles) => {
    setFiles(incomingFiles);
  };

  const onDelete = (id) => {
    setFiles(files.filter((x) => x.id !== id));
  };

  const materialOptions = useMemo(
    () =>
      materials.map((material) => ({
        value: material.id,
        label: material.name,
      })),
    [materials]
  );

  const gemOptions = useMemo(
    () =>
      gems.map((gem) => ({
        value: gem.id,
        label: gem.name,
      })),
    [gems]
  );

  return (
    <section className="mb-4">
      <StyledHeading className="h1-responsive font-weight-bold text-center my-4">
        Create Product
      </StyledHeading>

      <Row>
        <div className="col-md-12 mb-md-0 mb-5">
          <form
            id="contact-form"
            name="contact-form"
            onSubmit={(e) => {
              e.preventDefault();
              onSubmit();
            }}
          >
            <Row className="mb-2 p-3 justify-content-center">
              <div className="col-md-6 mb-5 mb-md-0">
                <div className="md-form mb-0">
                  <FloatingLabel label="Product name">
                    <Form.Control
                      type="text"
                      onChange={handleOnChange}
                      name="name"
                      placeholder="Product name"
                      value={productCreationData.name}
                    />
                  </FloatingLabel>
                </div>
              </div>
            </Row>
            <Row className="mb-2 p-3 justify-content-center">
              <div className="col-md-6 mb-5 mb-md-0">
                <div className="md-form mb-0">
                  <FloatingLabel label="Price">
                    <Form.Control
                      type="number"
                      onChange={handleOnChange}
                      name="price"
                      placeholder="Price"
                      value={productCreationData.price}
                    />
                  </FloatingLabel>
                </div>
              </div>
            </Row>
            <Row className="mb-2 p-3 justify-content-center">
              <div className="col-md-6 mb-5 mb-md-0">
                <div className="md-form mb-0">
                  <FloatingLabel label="Quantity">
                    <Form.Control
                      type="text"
                      onChange={handleOnChange}
                      name="quantity"
                      placeholder="Quantity"
                      value={productCreationData.quantity}
                    />
                  </FloatingLabel>
                </div>
              </div>
            </Row>
            <Row className="mb-2 p-3 justify-content-center">
              <div className="col-md-6 mb-5 mb-md-0">
                <div className="md-form mb-0">
                  Category
                  <Select
                    placeholder="Parent category"
                    onChange={onCategorySelect}
                    options={categories}
                    name="categoryId"
                    disabled={categories.length === 0}
                    theme={reactSelectTheme}
                    value={categories.find(
                      (cat) => cat.value === productCreationData.categoryId
                    )}
                  />
                </div>
              </div>
            </Row>
            <Row className="mb-2 p-3 justify-content-center">
              <div className="col-md-6 mb-5 mb-md-0">
                <div className="md-form mb-0">
                  Materials
                  <Select
                    aria-label=".form-select-sm example"
                    name="materials"
                    theme={reactSelectTheme}
                    isMulti
                    onChange={onMaterialsSelect}
                    options={materialOptions}
                    placeholder="Select materials..."
                    value={materialOptions.filter((option) =>
                      productCreationData.materials.includes(option.value)
                    )}
                  ></Select>
                </div>
              </div>
            </Row>

            <Row className="mb-2 p-3 justify-content-center">
              <div className="col-md-6 mb-5 mb-md-0">
                <div className="md-form mb-0">
                  Gems
                  <Select
                    aria-label=".form-select-sm example"
                    name="gems"
                    theme={reactSelectTheme}
                    isMulti
                    onChange={onGemsSelect}
                    options={gemOptions}
                    placeholder="Select gems..."
                    value={gemOptions.filter((option) =>
                      productCreationData.gems.includes(option.value)
                    )}
                  ></Select>
                </div>
              </div>
            </Row>
            <Row>
              <DropZoneContainer className="text-center">
                <div className="col-md-6 mb-5 mb-md-0">
                  <FileUpload
                    onDelete={onDelete}
                    onUpload={updateFiles}
                    files={files}
                  />
                </div>
              </DropZoneContainer>
            </Row>
          </form>

          <div className="text-center text-md-left p-3">
            <ResponsiveButton
              className="btn btn-primary"
              onClick={onSubmit}
              tabletWidth={30}
            >
              Create
            </ResponsiveButton>
          </div>
          <div className="status"></div>
        </div>
      </Row>
    </section>
  );
};

export default CreateProductForm;

const DropZoneContainer = styled.div`
  display: flex;
  justify-content: center;
`;
