import { useCallback, useState } from "react";
import { FloatingLabel, Form, Row } from "react-bootstrap";
import toast from "react-hot-toast";
import { createMaterial } from "../../apis/materials";
import {
  ResponsiveButton,
  StyledHeading,
} from "../commonComponents/CommonStyledComponents";
import { ChromePicker } from "react-color";
import styled from "styled-components";

const validateForm = (formValues) => {
  const errList = [];
  if (!formValues.name) {
    errList.push("Name is required!");
  }

  if (!formValues.color) {
    errList.push("Color is required!");
  }

  return errList;
};

const cover = {
  position: "fixed",
  top: "0px",
  right: "0px",
  bottom: "0px",
  left: "0px",
};

const CreateMaterialForm = () => {
  const [displayColorPicker, setDisplayColorPicker] = useState(false);
  const [materialCreationData, setMaterialCreationData] = useState({
    name: "",
    color: "",
  });

  const clearForm = () => {
    setMaterialCreationData({
      name: "",
      color: "",
    });
  };

  const onSubmit = useCallback(() => {
    const errors = validateForm(materialCreationData);
    if (errors.length > 0) {
      toast.error(errors.join("\n"));
      return;
    }

    toast.promise(createMaterial(materialCreationData), {
      loading: "Loading...",
      success: () => {
        clearForm();
        return "Successfully created!";
      },
      error: "Error while sending!",
    });
  }, [materialCreationData]);

  const handleOnChange = (e) => {
    toast.dismiss();
    setMaterialCreationData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  return (
    <section className="mb-4">
      <StyledHeading className="h1-responsive font-weight-bold text-center my-4">
        Create Material
      </StyledHeading>

      <Row>
        <div className="col-md-12 mb-md-0 mb-5">
          <form
            id="contact-form"
            name="contact-form"
            onSubmit={(e) => {
              e.preventDefault();
              onSubmit();
            }}
          >
            <div className="row mb-2 p-3">
              <div className="col-md-6 mb-5 mb-md-0">
                <div className="md-form mb-0">
                  <FloatingLabel label="Material name">
                    <Form.Control
                      type="text"
                      onChange={handleOnChange}
                      name="name"
                      placeholder="Material name"
                      value={materialCreationData.name}
                    />
                  </FloatingLabel>
                </div>
              </div>
              <div className="col-md-6">
                <div className="md-form mb-0">
                  <FloatingLabel label="Material color">
                    <Form.Control
                      type="text"
                      onClick={() => setDisplayColorPicker(true)}
                      onChange={() => null}
                      name="color"
                      placeholder="Material color"
                      value={materialCreationData.color}
                    ></Form.Control>
                    {displayColorPicker && (
                      <AbsoluteContainer>
                        <ChromePicker
                          color={materialCreationData.color}
                          styles={cover}
                          onChange={(color) =>
                            setMaterialCreationData((prevState) => ({
                              ...prevState,
                              color: color.hex,
                            }))
                          }
                        />
                      </AbsoluteContainer>
                    )}
                  </FloatingLabel>
                </div>
              </div>
            </div>
          </form>

          <div className="text-center text-md-left p-3">
            <ResponsiveButton
              className="btn btn-primary"
              onClick={onSubmit}
              tabletWidth={30}
            >
              Create
            </ResponsiveButton>
          </div>
          <div className="status"></div>
        </div>
      </Row>
    </section>
  );
};

export default CreateMaterialForm;

const AbsoluteContainer = styled.div`
  position: absolute;
  right: 0;
`;
