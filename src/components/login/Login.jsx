import { useCallback, useState } from "react";
import { Link } from "react-router-dom";
import { login } from "../../apis/authentication";
import "./Login.css";
import { ReactSession } from "react-client-session";
import styled from "styled-components";
import { NormalButton } from "../commonComponents/CommonStyledComponents";
import { theme } from "../../theme";
import toast from "react-hot-toast";

const Login = () => {
  const [showPassword, setShowPassword] = useState(false);
  const [loginData, setLoginData] = useState({
    email: "",
    password: "",
  });

  const handleOnChange = (e) => {
    setLoginData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const onSubmit = useCallback(
    (e) => {
      e.preventDefault();
      toast.promise(login(loginData), {
        loading: "Loading...",
        success: (response) => {
          ReactSession.set("token", response.data);
          return "Successfully created!";
        },
        error: (error) =>
          error?.response?.data?.join("\n") ?? "Error while sending!",
      });
    },
    [loginData]
  );

  return (
    <>
      <Container>
        <Heading className="text-center">Absolute Creations</Heading>
        <LoginDetails className="text-muted text-center pt-2">
          Enter your login details
        </LoginDetails>
        <form className="pt-3" onSubmit={onSubmit}>
          <div className="form-group py-2">
            <InputField>
              <span className="fa fa-user p-2"></span>
              <Input
                type="email"
                name="email"
                placeholder="Email Address"
                onChange={handleOnChange}
                value={loginData.email}
              />
            </InputField>
          </div>
          <div className="form-group py-1 pb-2">
            <InputField>
              <span className="fa fa-lock p-2"></span>
              <Input
                type={showPassword ? "text" : "password"}
                placeholder="Enter your Password"
                name="password"
                onChange={handleOnChange}
                value={loginData.password}
              />
              <button className="btn bg-white text-muted">
                <EyelashButton
                  className="fa fa-eye-slash"
                  onClick={(e) => {
                    e.preventDefault();
                    setShowPassword(!showPassword);
                  }}
                ></EyelashButton>
              </button>
            </InputField>
          </div>
          <LoginButton className="text-center my-3" onClick={onSubmit}>
            Log in
          </LoginButton>
          <div className="text-center pt-3 text-muted">
            Not a member? <SignUpLink to="/register">Sign up</SignUpLink>
          </div>
        </form>
      </Container>
    </>
  );
};

export default Login;

const Container = styled.div`
  max-width: 500px;
  border-radius: 10px;
  margin: 13rem auto;
  padding: 30px 40px;
  box-shadow: 20px 20px 80px ${theme.navBackgroundColor};

  @media (max-width: 575px) {
    .wrapper {
      margin: 10px;
    }
  }
  @media (max-width: 424px) {
    .wrapper {
      padding: 30px 10px;
      margin: 5px;
    }
  }
`;

const LoginDetails = styled.div`
  font-family: "Poppins", sans-serif;
  font-size: 1.5rem;
  line-height: 1.2;
`;

const Heading = styled.div`
  font-family: "Kaushan Script", cursive;
  font-size: 3.5rem;
  font-weight: bold;
  color: #400485;
  font-style: italic;
`;

const LoginButton = styled(NormalButton)`
  width: 100%;
  border-radius: 1.25rem;
  background-color: #400485;
`;

const SignUpLink = styled(Link)`
  text-decoration: none;
  color: #400485;
  font-weight: 700;
  :hover {
    text-decoration: none;
    color: #7b4ca0;
  }
`;

const InputField = styled.div`
  border-radius: 5px;
  padding: 5px;
  display: flex;
  align-items: center;
  cursor: pointer;
  border: 1px solid #400485;
  color: #400485;
  :hover {
    color: #7b4ca0;
    border: 1px solid #7b4ca0;
  }
`;

const EyelashButton = styled.span`
  border: none;
  outline: none;
  box-shadow: none;
`;

const Input = styled.input`
  border: none;
  outline: none;
  box-shadow: none;
  width: 100%;
  padding: 0px 0.125rem;
  font-family: "Poppins", sans-serif;
`;
