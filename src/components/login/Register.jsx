import { useCallback, useState } from "react";
import toast from "react-hot-toast";
import { Link, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { register } from "../../apis/register";
import { theme } from "../../theme";
import { NormalButton } from "../commonComponents/CommonStyledComponents";

const lowerCaseCharRegex = new RegExp("[a-z]");
const upperCaseCharRegex = new RegExp("[A-Z]");

const validate = (registrationData) => {
  const { password, passwordConfirm, firstName, lastName, email } =
    registrationData;

  const errorMessages = [];
  if (!firstName) {
    errorMessages.push("First name is required!");
  }
  if (!lastName) {
    errorMessages.push("Last name is required!");
  }
  if (!email) {
    errorMessages.push("Email is required!");
  }
  if (!(password.length >= 8 && password.length <= 16)) {
    errorMessages.push("Password should be between 8 and 16 characters!");
  }
  if (!upperCaseCharRegex.test(password)) {
    errorMessages.push(
      "Password should contain at least one upper case character!"
    );
  }
  if (!lowerCaseCharRegex.test(password)) {
    errorMessages.push(
      "Password should contain at least one lower case character!"
    );
  }
  if (password !== passwordConfirm) {
    errorMessages.push("Passwords should match!");
  }
  return errorMessages;
};

const Register = () => {
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [registrationData, setRegistrationData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    passwordConfirm: "",
  });
  let navigate = useNavigate();

  const handleOnChange = (e) => {
    setRegistrationData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const onSubmit = useCallback(
    (e) => {
      e.preventDefault();
      const errorMessages = validate(registrationData);
      if (errorMessages.length > 0) {
        toast.error(errorMessages.join("\n"));
        return;
      }
      toast.promise(register(registrationData), {
        loading: "Loading...",
        success: () => {
          navigate("/login");
          return "Successfully registered!";
        },
        error: (error) =>
          error?.response?.data?.join("\n") ?? "Error while sending!",
      });
    },
    [registrationData, navigate]
  );

  return (
    <>
      <Container>
        <Heading className="text-center">Absolute Creations</Heading>
        <LoginDetails className="text-muted text-center pt-2">
          Register
        </LoginDetails>
        <form className="pt-3" onSubmit={onSubmit}>
          <div className="form-group pt-1 pb-3">
            <InputField>
              <span className="fa fa-user p-2"></span>
              <Input
                type="text"
                placeholder="First name"
                name="firstName"
                onChange={handleOnChange}
                value={registrationData.firstName}
              />
            </InputField>
          </div>
          <div className="form-group pb-3">
            <InputField>
              <span className="fa fa-user p-2"></span>
              <Input
                type="text"
                placeholder="Last name"
                name="lastName"
                onChange={handleOnChange}
                value={registrationData.lastName}
              />
            </InputField>
          </div>
          <div className="form-group pb-3">
            <InputField>
              <span className="fa fa-envelope p-2"></span>
              <Input
                type="email"
                name="email"
                placeholder="Email address"
                onChange={handleOnChange}
                value={registrationData.email}
              />
            </InputField>
          </div>
          <div className="form-group pb-3">
            <InputField>
              <span className="fa fa-lock p-2"></span>
              <Input
                type={showPassword ? "text" : "password"}
                placeholder="Enter your password"
                name="password"
                onChange={handleOnChange}
                value={registrationData.password}
              />
              <button className="btn bg-white text-muted">
                <EyelashButton
                  className="fa fa-eye-slash"
                  onClick={(e) => {
                    e.preventDefault();
                    setShowPassword(!showPassword);
                  }}
                ></EyelashButton>
              </button>
            </InputField>
          </div>
          <div className="form-group pb-2">
            <InputField>
              <span className="fa fa-lock p-2"></span>
              <Input
                type={showConfirmPassword ? "text" : "password"}
                placeholder="Confirm password"
                name="passwordConfirm"
                onChange={handleOnChange}
                value={registrationData.passwordConfirm}
              />
              <button className="btn bg-white text-muted">
                <EyelashButton
                  className="fa fa-eye-slash"
                  onClick={(e) => {
                    e.preventDefault();
                    setShowConfirmPassword(!showConfirmPassword);
                  }}
                ></EyelashButton>
              </button>
            </InputField>
          </div>
          <LoginButton className="text-center my-3" onClick={onSubmit}>
            Register
          </LoginButton>
          <div className="text-center pt-3 text-muted">
            Already have an account? <SignUpLink to="/Login">Login</SignUpLink>
          </div>
        </form>
      </Container>
    </>
  );
};

export default Register;

const Container = styled.div`
  max-width: 500px;
  border-radius: 10px;
  margin: 9rem auto;
  padding: 30px 40px;
  box-shadow: 20px 20px 80px ${theme.navBackgroundColor};

  @media (max-width: 575px) {
    .wrapper {
      margin: 10px;
    }
  }
  @media (max-width: 424px) {
    .wrapper {
      padding: 30px 10px;
      margin: 5px;
    }
  }
`;

const LoginDetails = styled.div`
  font-family: "Poppins", sans-serif;
  font-size: 1.5rem;
  line-height: 1.2;
`;

const Heading = styled.div`
  font-family: "Kaushan Script", cursive;
  font-size: 3.5rem;
  font-weight: bold;
  color: #400485;
  font-style: italic;
`;

const LoginButton = styled(NormalButton)`
  width: 100%;
  border-radius: 1.25rem;
  background-color: #400485;
`;

const SignUpLink = styled(Link)`
  text-decoration: none;
  color: #400485;
  font-weight: 700;
  :hover {
    text-decoration: none;
    color: #7b4ca0;
  }
`;

const InputField = styled.div`
  border-radius: 5px;
  padding: 5px;
  display: flex;
  align-items: center;
  cursor: pointer;
  border: 1px solid #400485;
  color: #400485;
  :hover {
    color: #7b4ca0;
    border: 1px solid #7b4ca0;
  }
`;

const EyelashButton = styled.span`
  border: none;
  outline: none;
  box-shadow: none;
`;

const Input = styled.input`
  border: none;
  outline: none;
  box-shadow: none;
  width: 100%;
  padding: 0px 0.125rem;
  font-family: "Poppins", sans-serif;
`;
