import { Image } from "react-bootstrap";
import styled from "styled-components";
import { NormalButton } from "../commonComponents/CommonStyledComponents";
import { useCartContext } from "../contexts/CartContext";
import { fadeInAnimation } from "../../constants";

const ProductCard = ({ id, name, price, imageUrl }) => {
  const { addToCart } = useCartContext();

  return (
    <Container>
      <ProductImage src={imageUrl ?? "./images/ring.png"} thumbnail />
      <ProductContainer>
        <AddToProductButton onClick={() => addToCart(id)}>
          ADD TO CART
        </AddToProductButton>
        <div>
          <Name>{name}</Name>
          <Price>{price} лв.</Price>
        </div>
      </ProductContainer>
    </Container>
  );
};

export default ProductCard;

const ProductImage = styled(Image)`
  transition: opacity 0.25s ease-in-out;
  animation: 2s ${fadeInAnimation};
  height: 100%;
  max-height: 300px;
`;

const Container = styled.div`
  height: 400px;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
`;

const ProductContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const AddToProductButton = styled(NormalButton)`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  font-size: 0.9rem;
`;

const Name = styled.span`
  font-size: 0.9rem;
`;

const Price = styled.span`
  font-size: 0.9rem;
`;
