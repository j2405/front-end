import { useEffect, useMemo, useRef, useState } from "react";
import { OverlayTrigger, Row, Tooltip } from "react-bootstrap";
import {
  CursorCol,
  MaterialCircle,
  ResponsiveButton,
  StyledHeading,
  ThemedSpinner,
} from "../commonComponents/CommonStyledComponents";
import ProductCard from "./ProductCard";
import Select from "react-select";
import styled from "styled-components";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import { getAllCategories } from "../../apis/categories";
import { getAllGems } from "../../apis/gems";
import { useSearchParams } from "react-router-dom";

import { useProductsPaginated } from "../../hooks/useProductsPaginated";
import { fadeInAnimation } from "../../constants";
import { getPriceRangeForProducts } from "../../apis/products";
import { reactSelectTheme, theme } from "../../theme";
import { getAllMaterials } from "../../apis/materials";

export const selectOptions = [
  {
    value: { sortColumn: "dateOfAddition", sortDirection: "desc" },
    label: "Sort by date - Descending",
  },
  {
    value: { sortColumn: "dateOfAddition", sortDirection: "asc" },
    label: "Sort by date - Ascending",
  },
  {
    value: { sortColumn: "name", sortDirection: "desc" },
    label: "Sort by name - Descending",
  },
  {
    value: { sortColumn: "name", sortDirection: "asc" },
    label: "Sort by name - Ascending",
  },
  {
    value: { sortColumn: "price", sortDirection: "desc" },
    label: "Sort by price - Descending",
  },
  {
    value: { sortColumn: "price", sortDirection: "asc" },
    label: "Sort by price - Ascending",
  },
];

const productSizeOptions = [
  { value: 12, label: "12" },
  { value: 24, label: "24" },
  { value: 48, label: "48" },
  { value: 96, label: "96" },
];

const ProductsPage = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const {
    debouncedChangeHandler,
    onLoadMoreCLick,
    handleOnChange,
    page,
    loading,
    options,
  } = useProductsPaginated();

  const [priceRange, setPriceRange] = useState({ minPrice: 0, maxPrice: 0 });

  const [gems, setGems] = useState([]);
  const [materials, setMaterials] = useState([]);

  const [categories, setCategories] = useState([]);
  const prevRange = useRef(options.priceRange);

  useEffect(() => {
    if (prevRange.current === options.priceRange) {
      prevRange.current = options.priceRange;
      getPriceRangeForProducts({ ...options, categoryId: null }).then(
        (response) => setPriceRange(response.data)
      );
    }
  }, [options]);

  useEffect(() => {
    getAllMaterials().then((response) => setMaterials(response.data));
  }, [setMaterials]);

  useEffect(() => {
    getAllGems().then((response) =>
      setGems(response.data.map((gem) => ({ label: gem.name, value: gem.id })))
    );
  }, [setGems]);

  useEffect(() => {
    getAllCategories().then((response) => {
      const changedCategories = response.data.map((category) => ({
        value: category.id,
        label: category.name,
      }));
      setCategories(changedCategories);
    });
  }, [setCategories]);

  const selectCategories = useMemo(
    () => [{ value: null, label: "All Categories" }, ...categories],
    [categories]
  );

  const selectGems = useMemo(
    () => [{ value: null, label: "All Gems" }, ...gems],
    [gems]
  );
  return (
    <div>
      <StyledHeading className="h1-responsive font-weight-bold text-center my-4">
        Products
      </StyledHeading>

      <Filters className="mb-2 p-3 justify-content-between">
        <LeftOptions>
          <CategoriesSelect
            value={selectCategories.find(
              (category) => category.value === options.categoryId
            )}
            options={selectCategories}
            onChange={(newValue) => {
              if (newValue.value) {
                setSearchParams({
                  ...searchParams,
                  categoryId: newValue.value,
                });
              } else {
                setSearchParams({ ...searchParams });
              }
            }}
            theme={reactSelectTheme}
            placeholder="Select Category..."
          />
          {priceRange.minPrice !== 0 && (
            <Range
              priceRange={
                options.priceRange ?? {
                  min: priceRange.minPrice,
                  max: priceRange.maxPrice,
                }
              }
              handleOnChange={debouncedChangeHandler}
              minPrice={priceRange.minPrice}
              maxPrice={priceRange.maxPrice}
            />
          )}
        </LeftOptions>
        <RightOptions>
          <Select
            options={selectOptions}
            defaultValue={selectOptions[0]}
            theme={reactSelectTheme}
            onChange={(newValue) => handleOnChange("sorting", newValue.value)}
            className="w-75"
          />{" "}
          <SizeSelect
            options={productSizeOptions}
            defaultValue={productSizeOptions[0]}
            onChange={(newValue) => handleOnChange("pageSize", newValue.value)}
            theme={reactSelectTheme}
          />
        </RightOptions>
        <SecondaryFilters className="mt-2">
          <GemsSelect
            value={gems.find((gem) => gem.value === options.gem)}
            options={selectGems}
            defaultValue={selectGems[0]}
            onChange={(newValue) => {
              handleOnChange("gem", newValue.value);
            }}
            theme={reactSelectTheme}
            placeholder="Select Gem..."
          />
          <MaterialsContainer>
            Materials:{" "}
            {materials.map((material) => (
              <OverlayTrigger
                key={material.id}
                placement="top"
                overlay={<Tooltip key={material.id}>{material.name}</Tooltip>}
              >
                <MaterialCircle
                  key={material.id}
                  className="mx-2"
                  color={material.color}
                  isSelected={options.material === material.id}
                  onClick={() => {
                    if (options.material === material.id) {
                      handleOnChange("material", null);
                    } else {
                      handleOnChange("material", material.id);
                    }
                  }}
                />
              </OverlayTrigger>
            ))}
          </MaterialsContainer>
        </SecondaryFilters>
      </Filters>
      <Row>
        {page.products.map(({ id, name, price, imageUrls }) => (
          <CursorCol key={id + name} md="3" className="mb-5 px-3">
            <ProductCard
              name={name}
              price={price}
              id={id}
              key={id + name + price}
              imageUrl={imageUrls[0]}
            />
          </CursorCol>
        ))}
      </Row>

      {!loading && page.products.length === 0 && (
        <h4 className="text-center">No products found!</h4>
      )}

      <LoadMoreContainer>
        {loading ? (
          <ThemedSpinner animation="border" variant="primary" />
        ) : (
          <>
            {page.hasNext && (
              <LoadMoreButton onClick={onLoadMoreCLick}>
                Load more
              </LoadMoreButton>
            )}
          </>
        )}
      </LoadMoreContainer>
    </div>
  );
};

export default ProductsPage;

const Range = ({ maxPrice, minPrice, handleOnChange, priceRange }) => (
  <InputRange
    formatLabel={(value) => `${value}лв`}
    value={priceRange}
    maxValue={maxPrice}
    minValue={minPrice}
    step={2}
    onChange={(value) => handleOnChange("priceRange", value)}
  />
);

const LeftOptions = styled.div`
  display: flex;
  width: 50%;
  justify-content: space-between;
  align-items: center;

  .input-range__label--min {
    display: none;
  }

  .input-range__label--max {
    display: none;
  }

  .input-range {
    width: 50%;
  }

  .input-range__track--active {
    background: ${theme.rangeColor};
  }

  .input-range__slider {
    background: ${theme.rangeColor};
  }

  .input-range__label-container {
    font-size: 1.125rem;
    bottom: 0.5rem;
  }
`;

const RightOptions = styled.div`
  display: flex;
  justify-content: space-between;
  width: 40%;
`;

const SizeSelect = styled(Select)`
  width: 22%;
`;

const CategoriesSelect = styled(Select)`
  width: 40%;
`;

const LoadMoreButton = styled(ResponsiveButton)`
  width: 30%;
`;

const LoadMoreContainer = styled(Row)`
  justify-content: center;
`;

const Filters = styled(Row)`
  animation: 2s ${fadeInAnimation};
`;

const SecondaryFilters = styled(Row)`
  display: flex;
  animation: 2s ${fadeInAnimation};
`;

const MaterialsContainer = styled.div`
  display: flex;
  align-items: center;
  width: 78%;
`;

const GemsSelect = styled(Select)`
  width: 22%;
`;
