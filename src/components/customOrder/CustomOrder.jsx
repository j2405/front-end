import { useCallback, useState } from "react";
import { FloatingLabel, Form } from "react-bootstrap";
import styled from "styled-components";
import { customOrderEmail } from "../../apis/emails";
import { breakpointsMedia, isMobile, zoomInAnimation } from "../../constants";

import toast from "react-hot-toast";
import FileUpload from "../commonComponents/FileUpload";
import { useNavigate } from "react-router-dom";
import {
  ResponsiveButton,
  StyledHeading,
} from "../commonComponents/CommonStyledComponents";

const CustomOrder = () => {
  const [files, setFiles] = useState([]);
  const [message, setMessage] = useState("");

  const navigate = useNavigate();

  const onSubmit = useCallback(() => {
    if (message.length < 50) {
      toast.error("Details should be at least 50 chars!");
      return;
    }

    toast.promise(
      customOrderEmail({ message, files: files.map((file) => file.file) }),
      {
        loading: "Loading...",
        success: () => {
          navigate("/checkout/success");
          return "Successfully sent!";
        },
        error: "Error while sending!",
      }
    );
  }, [message, files, navigate]);

  const handleOnChange = (e) => {
    toast.dismiss();
    setMessage(e.target.value);
  };

  const updateFiles = (incomingFiles) => {
    setFiles(incomingFiles);
  };

  const onDelete = (id) => {
    setFiles(files.filter((x) => x.id !== id));
  };

  return (
    <Section className="mb-4">
      <StyledHeading className="h1-responsive font-weight-bold text-center my-4">
        Make a custom order
      </StyledHeading>

      <p className="text-center w-responsive mx-auto mb-5">
        Do you have any idea for jewelry in mind? Please do not hesitate to make
        an order directly through this form. Our team will come back to you
        within a few days to help you and discuss the design.
      </p>

      <div className="row">
        <div className="col-md-7 mb-md-0 mb-5">
          <form
            id="contact-form"
            name="contact-form"
            action="mail.php"
            method="POST"
          >
            <div className="row mb-2 p-3">
              <div className="col-md-12">
                <div className="md-form">
                  <FloatingLabel
                    controlId="floatingTextarea2"
                    label="Details For Order"
                  >
                    <Form.Control
                      as="textarea"
                      style={{ height: "10rem" }}
                      onChange={handleOnChange}
                      name="message"
                      placeholder="Details For Order"
                    />
                  </FloatingLabel>
                </div>
              </div>
            </div>
          </form>

          {isMobile && (
            <DropZoneContainer className="col-md-12 text-center p-3">
              <FileUpload
                onDelete={onDelete}
                onUpload={updateFiles}
                files={files}
              />
            </DropZoneContainer>
          )}

          <div className="text-center text-md-left p-3">
            <SendButton className="btn btn-primary" onClick={onSubmit}>
              Make order
            </SendButton>
          </div>
          <div className="status"></div>
        </div>
        {!isMobile && (
          <DropZoneContainer className="col-md-5 text-center">
            <FileUpload
              onDelete={onDelete}
              onUpload={updateFiles}
              files={files}
            />
          </DropZoneContainer>
        )}
      </div>
    </Section>
  );
};

export default CustomOrder;

const DropZoneContainer = styled.div`
  display: flex;
  justify-content: center;
`;

const SendButton = styled(ResponsiveButton)`
  width: 100%;

  ${breakpointsMedia.tablet} {
    width: 30%;
  }
`;

const Section = styled.section`
  animation: 1s ${zoomInAnimation};
`;
