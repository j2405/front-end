import { useEffect, useState } from "react";
import { Col, Container, Image, Row } from "react-bootstrap";
import styled from "styled-components";
import { getAllProducts } from "../../apis/products";
import { theme } from "../../theme";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {
  NormalButton,
  StyledHeading,
} from "../commonComponents/CommonStyledComponents";
import ProductCard from "../products/ProductCard";
import { Carousel } from "react-responsive-carousel";
import { useNavigate } from "react-router-dom";
import { getCategoryByName } from "../../apis/categories";

const Home = () => {
  const navigate = useNavigate();
  const [ringsCategoryId, setRingsCategoryId] = useState(null);
  const [products, setProducts] = useState([]);

  useEffect(() => {
    getCategoryByName("Rings").then((response) =>
      setRingsCategoryId(response.data.id)
    );
  }, [setRingsCategoryId]);

  useEffect(() => {
    getAllProducts({
      pageSize: 8,
      page: 0,
      sorting: { sortColumn: "dateOfAddition", sortDirection: "desc" },
    }).then((response) => setProducts(response.data.products));
  }, [setProducts]);

  return (
    <Container>
      <StyledHeading className="h1-responsive font-weight-bold text-center my-4">
        Absolute creations
      </StyledHeading>

      <Carousel
        showArrows={false}
        infiniteLoop
        // autoPlay
        stopOnHover
        swipeable
        showStatus={false}
        showThumbs={false}
        interval={5000}
        transitionTime={750}
        renderItem={(item, options) => options.isSelected && item}
      >
        <CarouselContainer>
          <Image src={"./images/slider2.webp"} />
          <AbsolutePositionedOnCarousel>
            <ExclusiveOffersParagraph>
              exclusive rings this week
            </ExclusiveOffersParagraph>
            <CarouselHeading>Engagement rings</CarouselHeading>
            <ShoppingNowButton
              onClick={() =>
                navigate(`/products?categoryId=${ringsCategoryId}`)
              }
            >
              SHOPPING NOW
            </ShoppingNowButton>
          </AbsolutePositionedOnCarousel>
        </CarouselContainer>
        <CarouselContainer>
          <Image src={"./images/slider1.webp"} />
          <AbsolutePositionedOnCarousel>
            <ExclusiveOffersParagraph>
              new arrival every week
            </ExclusiveOffersParagraph>
            <CarouselHeading>Jewelry arrivals</CarouselHeading>
            <ShoppingNowButton onClick={() => navigate("/products")}>
              SHOPPING NOW
            </ShoppingNowButton>
          </AbsolutePositionedOnCarousel>
        </CarouselContainer>
      </Carousel>

      <TabContainer>
        <NewArrivalsTab>New Arrivals</NewArrivalsTab>
      </TabContainer>

      <Row>
        {products.map(({ id, name, price, imageUrls }) => (
          <CursorCol key={id} md="3" className="mb-5 px-3">
            <ProductCard
              name={name}
              price={price}
              id={id}
              key={id + name + price}
              imageUrl={imageUrls[0]}
            />
          </CursorCol>
        ))}
      </Row>
    </Container>
  );
};

export default Home;

const CursorCol = styled(Col)`
  &:hover > img {
    opacity: 0.5;
  }
`;

const TabContainer = styled.div`
  text-align: center;
  margin-top: 2.5rem;
  margin-bottom: 1.5rem;
`;

const NewArrivalsTab = styled.div`
  margin: 0 auto;
  border: 2px solid ${theme.homeNewArrivalsTabBorderColor};
  color: ${theme.homeNewArrivalsTabColor};
  display: inline-block;
  position: relative;
  padding: 0 1.8rem;
  font-family: "Playfair Display", serif;
  font-size: 1.4rem;
  line-height: 3rem;
  text-transform: capitalize;
  font-weight: 700;
  cursor: pointer;

  ::before {
    content: "";
    width: 200px;
    height: 2px;
    position: absolute;
    right: 100%;
    top: 50%;
    background-image: linear-gradient(
      to right,
      transparent,
      ${theme.homeNewArrivalsTabColor}
    );
  }

  ::after {
    content: "";
    width: 200px;
    height: 2px;
    position: absolute;
    left: 100%;
    top: 50%;
    background-image: linear-gradient(
      to right,
      ${theme.homeNewArrivalsTabColor},
      transparent
    );
  }
`;

const CarouselContainer = styled.div`
  position: relative;
`;

const AbsolutePositionedOnCarousel = styled.div`
  position: absolute;
  top: 3rem;
  left: 4rem;
  text-align: left;
`;

const ShoppingNowButton = styled(NormalButton)`
  padding: 0 2.2rem;
  margin-top: 7rem;
  font-weight: 500;
  height: 3rem;
  line-height: 3rem;
  font-size: 0.8rem;
  animation-name: fadeInLeft;
  animation-fill-mode: both;
  animation-duration: 1s;
  animation-delay: 0.6s;
`;

const CarouselHeading = styled.h1`
  margin-top: 0.5rem;
  font-size: 3rem;
  line-height: 3rem;
  font-weight: 700;
  color: #212121;
  text-transform: capitalize;
  font-family: "Playfair Display", serif;

  animation-name: fadeInLeft;
  animation-fill-mode: both;
  animation-duration: 1s;
  animation-delay: 0.6s;
`;

const ExclusiveOffersParagraph = styled.p`
  font-family: "Rubik", sans-serif;
  font-size: 1.1rem;
  line-height: 1.1rem;
  font-weight: 400;
  color: #242424;
  margin-top: 2rem;
  margin-bottom: 0;
  text-transform: capitalize;
  animation-name: fadeInLeft;
  animation-fill-mode: both;
  animation-duration: 1s;
  animation-delay: 0.6s;
`;
