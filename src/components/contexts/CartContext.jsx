import React, { useCallback, useContext, useState } from "react";
import { ReactSession } from "react-client-session";

const CartContext = React.createContext();

export const CartProvider = ({ children }) => {
  let addedToCartItems = ReactSession.get("products");
  const [items, setItems] = useState(addedToCartItems ? addedToCartItems : []);

  const addToCart = useCallback(
    (id) => {
      const newProducts = items.length ? [...items, id] : [id];

      ReactSession.set("products", newProducts);
      setItems(newProducts);
    },
    [setItems, items]
  );

  const clearCart = useCallback(() => {
    ReactSession.set("products", []);
    setItems([]);
  }, []);

  const setCart = useCallback((items) => {
    ReactSession.set("products", items);
    setItems(items);
  }, []);

  return (
    <CartContext.Provider
      value={{
        cartItems: items,
        addToCart,
        clearCart,
        setCart,
      }}
    >
      {children}
    </CartContext.Provider>
  );
};

export const useCartContext = () => useContext(CartContext);
