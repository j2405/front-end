import { Button, Col, Spinner } from "react-bootstrap";
import { Link } from "react-router-dom";
import styled, { css } from "styled-components";
import { breakpointsMedia } from "../../constants";
import { theme } from "../../theme";

export const NormalButton = styled(Button)`
  color: ${theme.buttonsColor};
  background-color: ${theme.buttonsBackgroundColor};
  border-color: ${theme.buttonsBorderColor};

  :hover {
    color: ${theme.buttonsHoverColor};
    background-color: ${theme.buttonsHoverBackgroundColor};
    border-color: ${theme.buttonsBorderHoverColor};
  }

  :focus {
    color: ${theme.buttonsHoverColor};
    background-color: ${theme.buttonsHoverBackgroundColor};
  }
`;

export const ResponsiveButton = styled(NormalButton)`
  width: 100%;

  ${breakpointsMedia.tablet} {
    width: ${({ tabletWidth }) => tabletWidth}%;
  }
`;

export const HooverableIcon = styled.i`
  :hover {
    color: #b2c2d0;
    cursor: pointer;
  }
`;

export const HooverableLink = styled(Link)`
  :hover {
    color: #e7dbcb;
  }

  :hover > * {
    color: #e7dbcb;
  }
`;

export const CursorCol = styled(Col)`
  & > img:hover {
    opacity: 0.5;
    cursor: pointer;
  }
`;

export const ThemedSpinner = styled(Spinner)`
  width: 4rem;
  height: 4rem;
  color: #5469d4 !important;
`;

export const StyledHeading = styled.h1`
  font-family: "Smooch", serif;
  font-size: 3.5rem;
`;

export const MaterialCircle = styled.div`
  display: inline-block;
  background-color: ${({ color }) => color};
  border: 0.1rem solid black;
  border-radius: 50%;
  width: 1.75rem;
  height: 1.75rem;
  box-shadow: 0 0 0 1px rgb(0 0 0 / 40%);
  ${({ isSelected }) =>
    isSelected &&
    css`
      border: 0.2rem solid ${theme.buttonsBorderHoverColor};
    `};
  :hover {
    cursor: pointer;
    border-color: ${theme.buttonsBorderHoverColor};
  }
`;
