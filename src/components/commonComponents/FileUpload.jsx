import { useState } from "react";
import { Dropzone, FileItem, FullScreenPreview } from "@dropzone-ui/react";

const FileUpload = ({ files, onUpload, onDelete }) => {
  const [imageSrc, setImageSrc] = useState(undefined);

  const handleSee = (imageSource) => {
    setImageSrc(imageSource);
  };

  return (
    <Dropzone
      onChange={onUpload}
      value={files}
      accept={"image/jpeg"}
      label={"Drop Files here or click to browse"}
      minHeight={"195px"}
      maxHeight={"500px"}
      disableScroll
    >
      {files.map((file) => (
        <FileItem
          {...file}
          key={file.id}
          onDelete={onDelete}
          onSee={handleSee}
          alwaysActive
          preview
          hd
        />
      ))}
      <FullScreenPreview
        imgSource={imageSrc}
        openImage={imageSrc}
        onClose={() => handleSee(undefined)}
      />
    </Dropzone>
  );
};

export default FileUpload;
