import { OverlayTrigger, Tooltip } from "react-bootstrap";
import styled from "styled-components";
import {
  companyEmail,
  facebookName,
  instagramName,
  phoneNumber,
} from "../../constants";
import { theme } from "../../theme";
import "./footer.css";

const Footer = () => {
  return (
    <Container className="footer-dark mt-3 fixed-bottom">
      <footer>
        <div className="container">
          <div className="row">
            <div className="col item social">
              <OverlayTrigger
                placement="top"
                overlay={<Tooltip>Facebook</Tooltip>}
              >
                <IconLink
                  href={`https://www.facebook.com/${facebookName}`}
                  className="me-3"
                >
                  <CircleIcon className="fa fa-facebook"></CircleIcon>
                </IconLink>
              </OverlayTrigger>
              <OverlayTrigger
                placement="top"
                overlay={<Tooltip>Instagram</Tooltip>}
              >
                <IconLink
                  className="me-3"
                  href={`https://www.instagram.com/${instagramName}`}
                >
                  <CircleIcon className="fa fa-instagram"></CircleIcon>
                </IconLink>
              </OverlayTrigger>
              <OverlayTrigger
                placement="top"
                overlay={<Tooltip>{phoneNumber}</Tooltip>}
              >
                <CircleIcon className="fa fa-phone me-3"></CircleIcon>
              </OverlayTrigger>
              <OverlayTrigger
                placement="top"
                overlay={<Tooltip>{companyEmail}</Tooltip>}
              >
                <CircleIcon className="fa fa-envelope"></CircleIcon>
              </OverlayTrigger>
            </div>
          </div>
          <p className="copyright">Absolute Creations © 2022</p>
        </div>
      </footer>
    </Container>
  );
};
//   <footer className="my-5 pt-5 text-muted text-center text-small">
//     <p className="mb-1">&copy; 2022-2023 absolute-creations.com</p>
//     {/* <ul className="list-inline">
//   <li className="list-inline-item">
//     <a href="#">Privacy</a>
//   </li>
//   <li className="list-inline-item">
//     <a href="#">Terms</a>
//   </li>
//   <li className="list-inline-item">
//     <a href="#">Support</a>
//   </li>
// </ul> */}
//   </footer>

export default Footer;

const CircleIcon = styled.i`
  font-size: 0.9rem;
  width: 2.25rem;
  height: 2.25rem;
  line-height: 2.25rem;
  display: inline-block;
  text-align: center;
  border-radius: 50%;
  box-shadow: 0 0 0 1px rgb(0 0 0 / 40%);
  color: black;
  opacity: 0.75;
  :hover {
    cursor: pointer;
    color: ${theme.footerIconHoverColor};
    box-shadow: 0 0 0 1px ${theme.footerIconHoverColor};
    opacity: 0.9;
  }
`;

const IconLink = styled.a`
  margin: 0;
  opacity: 0.8;
  color: #fff;
  :hover {
    color: #fff;
    opacity: 1;
  }
`;

const Container = styled.div`
  background-color: ${theme.footerBackgroundColor};
  border-top: 1px solid #ebebeb;
`;
