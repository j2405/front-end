import { useEffect, useState } from "react";
import Modal from "react-overlays/Modal";
import styled from "styled-components";
import { getAllOrderedProducts } from "../../apis/orderedProducts";
import { HooverableIcon } from "../commonComponents/CommonStyledComponents";
import OrderOverlayProduct from "./OrderOverlayProduct";

const OrderOverlay = ({
  addresses,
  hideOverlay,
  orderNumber,
  id,
  description,
}) => {
  const renderBackdrop = (props) => <Backdrop {...props} />;

  const [orderedProducts, setOrderedProducts] = useState([]);

  useEffect(() => {
    getAllOrderedProducts(id).then((response) =>
      setOrderedProducts(response.data)
    );
  }, [setOrderedProducts, id]);

  return (
    <RandomlyPositionedModal
      show
      onHide={hideOverlay}
      renderBackdrop={renderBackdrop}
      aria-labelledby="modal-label"
    >
      <div className="row">
        <div className="row mb-3 d-flex justify-content-between align-items-center">
          <span className={"col-md-11 fw-bold"}>
            Order number: {orderNumber}
          </span>

          <CloseButton
            className="fa fa-times text-end"
            aria-hidden="true"
            onClick={hideOverlay}
          />
        </div>
        {addresses &&
          addresses.map(
            ({
              firstName,
              lastName,
              address,
              address2,
              shippingAddress,
              phone,
              city,
              country,
              zip,
            }) => (
              <div
                className={"col-md-6 mb-2"}
                key={firstName + lastName + phone}
              >
                <AddressParagraph className="lead mb-1">
                  {shippingAddress ? "Shipping Address" : "Billing Address"}
                </AddressParagraph>
                <p className="mb-1">
                  {firstName} {lastName}
                </p>
                <p className="mb-1">{phone}</p>
                <p className="mb-1">
                  {country}, {city} {zip}
                </p>
                <p className="mb-1">
                  {address} {address2}
                </p>
              </div>
            )
          )}
        {description && (
          <>
            <div className="col-md-12 fw-bold">Description:</div>
            <p>{description}</p>
          </>
        )}
        {orderedProducts.length > 0 &&
          orderedProducts.map((orderedProduct) => (
            <OrderOverlayProduct {...orderedProduct} key={orderedProduct.id} />
          ))}
        {orderedProducts.length > 0 && (
          <div className="col-md-12 fw-bold">
            Total:{" "}
            {orderedProducts.reduce(
              (prevValue, current) =>
                prevValue + current.count * current.priceAtPayment,
              0
            )}{" "}
            лв
          </div>
        )}
      </div>
    </RandomlyPositionedModal>
  );
};
export default OrderOverlay;

const RandomlyPositionedModal = styled(Modal)`
  position: fixed;
  z-index: 1040;
  left: 25%;
  transform: translateX(-50%);
  top: 50%;
  transform: translateY(-50%);
  border: 1px solid #e5e5e5;
  background-color: white;
  box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5);
  padding: 1.25rem;
  width: 50%;
`;

const Backdrop = styled.div`
  position: fixed;
  z-index: 1040;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: #000;
  opacity: 0.5;
`;

const CloseButton = styled(HooverableIcon)`
  width: 5%;
  padding: 0;
`;

const AddressParagraph = styled.p`
  font-weight: 500;
`;
