import { useEffect, useState } from "react";
import { Image } from "react-bootstrap";
import styled from "styled-components";
import { getProduct } from "../../apis/products";

const OrderOverlayProduct = ({ productId, count, priceAtPayment }) => {
  const [product, setProduct] = useState(null);

  useEffect(() => {
    getProduct(productId).then((response) => setProduct(response.data));
  }, [setProduct, productId]);

  return (
    <>
      {product && (
        <div className="card rounded-3 mb-1 w-75">
          <div className="card-body p-1" onClick={() => null}>
            <Container>
              <Picture>
                <FixedHeightImage
                  src={product.imageUrls[0] || "./images/ring.png"}
                  fluid
                />
              </Picture>
              <Name>{product.name}</Name>
              <CategoryName>{product.category.name}</CategoryName>

              <Price className="text-muted">
                {count} X {priceAtPayment} лв
              </Price>
            </Container>
          </div>
        </div>
      )}
    </>
  );
};

export default OrderOverlayProduct;

const Container = styled.div`
  display: flex;
  align-items: center;
  gap: 1rem;
`;

const Name = styled.span`
  width: 40%;
`;

const CategoryName = styled.span`
  width: 25%;
`;

const Price = styled.span`
  width: 23%;
`;

const Picture = styled.span`
  height: 60px;
  width: 10%;
`;

const FixedHeightImage = styled(Image)`
  height: 100%;
  max-height: 60px;
`;
