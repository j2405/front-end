import { Button } from "react-bootstrap";
import moment from "moment";
import styled from "styled-components";
import OrderOverlay from "./OrderOverlay";
import { useCallback, useState } from "react";
import toast from "react-hot-toast";
import { cancelOrder, updateOrderStatus } from "../../apis/orders";
import { getAllStatusesAsSelectOptions, StatusEnum } from "../../enums/status";
import Select from "react-select";
import { fadeInAnimation } from "../../constants";
import { reactSelectTheme } from "../../theme";

const OrderCard = ({
  id,
  orderNumber,
  status,
  addresses,
  updatedOn,
  createdOn,
  refetch,
  description,
}) => {
  const [show, setShow] = useState(false);

  const hideOverlay = useCallback(() => setShow(false), [setShow]);

  const onCancelClick = useCallback(() => {
    toast.promise(cancelOrder(id), {
      loading: "Loading...",
      success: () => {
        refetch();
        return "Successfully cancelled!";
      },
      error: "Error while cancelling!",
    });
  }, [id, refetch]);

  const onStatusChange = useCallback(
    (newValue) => {
      toast.promise(updateOrderStatus(id, newValue.value), {
        loading: "Loading...",
        success: () => {
          refetch();
          return "Successfully changed status!";
        },
        error: "Error while changing status!",
      });
    },
    [id, refetch]
  );

  const statusOptions = getAllStatusesAsSelectOptions();

  const isAdmin = true;

  return (
    <CardContainer className="card rounded-3 mb-4" status={status.status}>
      <div className="card-body p-4" onClick={() => setShow(true)}>
        <div className="row d-flex justify-content-between align-items-center">
          <div className="col-md-4 ">
            <p className="lead fw-normal mb-1">Order number: {orderNumber}</p>
          </div>
          <div className="col-md-3">
            <p className="mb-1">
              Created on: {moment(createdOn).format("MM/DD/YYYY HH:mm")}
            </p>
            <p className="mb-1">
              Updated on: {moment(updatedOn).format("MM/DD/YYYY HH:mm")}
            </p>
          </div>

          <div
            className="col-md-2"
            onClick={(e) => {
              if (isAdmin && e.stopPropagation) {
                e.stopPropagation();
              }
            }}
          >
            {isAdmin ? (
              <Select
                className="custom-select d-block w-100"
                id="city"
                required
                options={statusOptions}
                value={statusOptions.find(
                  (option) => option.value === status?.status
                )}
                onChange={onStatusChange}
                theme={reactSelectTheme}
              />
            ) : (
              <h5 className="mb-0">{status.status}</h5>
            )}
          </div>
          <div className="col-md-2 text-end">
            {status.canCancelOrder && (
              <Button
                variant="danger"
                className="px-2"
                onClick={(e) => {
                  if (e.stopPropagation) {
                    e.stopPropagation();
                  }
                  onCancelClick();
                }}
              >
                Cancel Order
              </Button>
            )}
          </div>
        </div>
      </div>
      {show && (
        <OrderOverlay
          key={orderNumber}
          addresses={addresses}
          hideOverlay={hideOverlay}
          orderNumber={orderNumber}
          id={id}
          description={description}
        />
      )}
    </CardContainer>
  );
};

export default OrderCard;

const CardContainer = styled.div`
  border-color: ${({ status }) => status === StatusEnum.CANCELLED && "#f3594e"};
  border-color: ${({ status }) =>
    status === StatusEnum.IN_PROGRESS && "#ffe599"};
  border-color: ${({ status }) => status === StatusEnum.DELIVERED && "#8fce00"};
  :hover {
    cursor: pointer;
    background-color: #f5f5f5;
  }
  animation: 2s ${fadeInAnimation};
`;
