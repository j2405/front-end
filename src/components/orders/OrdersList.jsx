import { Row } from "react-bootstrap";
import styled from "styled-components";
import { orderSortingOptions } from "../../apis/orders";
import OrderCard from "./OrderCard";
import Select from "react-select";
import { getAllStatusesAsSelectOptions } from "../../enums/status";
import { useOrdersPaginated } from "../../hooks/useOrdersPaginated";
import {
  ResponsiveButton,
  StyledHeading,
  ThemedSpinner,
} from "../commonComponents/CommonStyledComponents";
import { fadeInAnimation } from "../../constants";
import { reactSelectTheme } from "../../theme";

const OrdersList = () => {
  const {
    orders,
    handleOnChange,
    fetchAndSetOrders,
    options,
    loading,
    onLoadMoreCLick,
    hasNext,
    debouncedChangeHandler,
  } = useOrdersPaginated();

  const statusOptions = [
    { label: "All statuses", value: null },
    ...getAllStatusesAsSelectOptions(),
  ];
  return (
    <section className="h-100">
      <div className="container h-100 py-3">
        <div className="row d-flex justify-content-center align-items-center h-100">
          <div className="col-12">
            <div className="d-flex justify-content-between align-items-center mb-4">
              <StyledHeading className="fw-normal mb-0 text-black text-center w-100">
                Orders List
              </StyledHeading>
            </div>

            <FiltersContainer className="mb-3">
              <LeftOptions>
                <Select
                  options={statusOptions}
                  value={statusOptions.find(
                    (option) => option.value === options.status
                  )}
                  onChange={(newValue) =>
                    handleOnChange("status", newValue.value)
                  }
                  placeholder="All statuses..."
                  theme={reactSelectTheme}
                />
              </LeftOptions>
              <OrderNumberContainer>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Order number..."
                  aria-label="Order number"
                  onChange={(e) =>
                    debouncedChangeHandler("orderNumber", e.target.value)
                  }
                />
              </OrderNumberContainer>
              <RightOptions>
                <Select
                  options={orderSortingOptions}
                  defaultValue={orderSortingOptions[0]}
                  onChange={(newValue) =>
                    handleOnChange("sorting", newValue.value)
                  }
                  theme={reactSelectTheme}
                />
              </RightOptions>
            </FiltersContainer>

            {orders.length > 0 &&
              orders.map((order) => (
                <OrderCard
                  {...order}
                  key={order.orderNumber}
                  refetch={fetchAndSetOrders}
                />
              ))}

            <LoadMoreContainer>
              {loading ? (
                <ThemedSpinner animation="border" variant="primary" />
              ) : (
                <>
                  {hasNext && (
                    <LoadMoreButton onClick={onLoadMoreCLick}>
                      Load more
                    </LoadMoreButton>
                  )}
                </>
              )}
            </LoadMoreContainer>

            {!loading && orders.length === 0 && (
              <h4 className="text-center">No orders found!</h4>
            )}
          </div>
        </div>
      </div>
    </section>
  );
};

export default OrdersList;

const RightOptions = styled.div`
  margin-left: auto;
  width: 30%;
`;

const LeftOptions = styled.div`
  width: 20%;
`;

const LoadMoreButton = styled(ResponsiveButton)`
  width: 30%;
`;

const LoadMoreContainer = styled(Row)`
  justify-content: center;
`;

const OrderNumberContainer = styled.div`
  width: 20%;
`;

const FiltersContainer = styled(Row)`
  animation: 2s ${fadeInAnimation};
`;
