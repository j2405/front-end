import { useEffect } from "react";
import { Image } from "react-bootstrap";
import styled from "styled-components";
import { useNavigate } from "react-router-dom";
import { fadeInAnimation } from "../../constants";

const SuccessfulOrder = () => {
  const navigate = useNavigate();

  useEffect(() => {
    const interval = setInterval(() => {
      navigate("/previous-orders");
    }, 8000);

    return () => clearInterval(interval);
  }, [navigate]);

  return (
    <div>
      <SuccessContainer>
        <h2>Your order has been successfully received.</h2>
        <h3>
          One of our agents will content you as soon as possible to confirm your
          order.
        </h3>
        <Image className="w-50" src="/images/success.jpg" fluid />
      </SuccessContainer>
    </div>
  );
};

export default SuccessfulOrder;

const SuccessContainer = styled.div`
  margin-top: 2rem;
  align-items: center;
  text-align: center;
  display: flex;
  flex-direction: column;
  animation: 2s ${fadeInAnimation};
`;
