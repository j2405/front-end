import styled from "styled-components";
import { slideInUpAnimation } from "../../constants";
import CheckoutProduct from "./CheckoutProduct";

const CheckoutCartProducts = ({ cartProductsInfo }) => {
  return (
    <Container className="col-md-4 order-md-2 mb-4">
      <h4 className="d-flex justify-content-between align-items-center mb-3">
        <span className="text-muted">Your cart</span>
        <span className="badge badge-secondary badge-pill">3</span>
      </h4>
      <ul className="list-group mb-3">
        {cartProductsInfo.products.map(({ productName, price, count }) => (
          <CheckoutProduct
            productName={productName}
            price={price}
            count={count}
            key={productName}
          />
        ))}
        {/* <li className="list-group-item d-flex justify-content-between bg-light">
                  <div className="text-success">
                    <h6 className="my-0">Promo code</h6>
                    <small>EXAMPLECODE</small>
                  </div>
                  <span className="text-success">-$5</span>
                </li> */}
        <li className="list-group-item d-flex justify-content-between">
          <span>Total (BGN)</span>
          <strong>{cartProductsInfo.total} лв</strong>
        </li>
      </ul>
      {/* <form className="card p-2">
                <div className="input-group">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Promo code"
                  />
                  <div className="input-group-append">
                    <button type="button" className="btn btn-secondary">
                      Redeem
                    </button>
                  </div>
                </div>
              </form> */}
    </Container>
  );
};

export default CheckoutCartProducts;

const Container = styled.div`
  animation: 2s ${slideInUpAnimation};
`;
