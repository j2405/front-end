import styled from "styled-components";

const CheckoutProduct = ({ productName, price, count }) => {
  return (
    <li className="list-group-item d-flex justify-content-between lh-condensed">
      <ProductDescription>
        <h6 className="my-0">{productName}</h6>
        {/* <small className="text-muted">Brief description</small> */}
      </ProductDescription>
      <span className="text-muted">
        {count} X {price} лв
      </span>
    </li>
  );
};

export default CheckoutProduct;

const ProductDescription = styled.div`
  display: flex;
  align-items: center;
  width: 60%;
  padding-right: 0.5rem;
`;
