import styled from "styled-components";
import Select from "react-select";
import { Country, City } from "country-state-city";
import { useCallback, useEffect, useMemo, useState } from "react";
import CheckoutCartProducts from "./CheckoutCartProducts";
import { getCartProductInfo } from "../../apis/products";
import { useCartContext } from "../contexts/CartContext";
import toast from "react-hot-toast";
import { createOrder } from "../../apis/orders";
import { useNavigate } from "react-router-dom";
import { isOnlyLetters, isOnlyNumbers, slideAnimation } from "../../constants";
import {
  StyledHeading,
  NormalButton,
} from "../commonComponents/CommonStyledComponents";
import { postcodeValidator } from "postcode-validator";
import { phone } from "phone";
import { Form } from "react-bootstrap";

const getCountryOptions = () => {
  return Country.getAllCountries().map((country) => ({
    value: country,
    label: country.name,
  }));
};

const getCitiesOptionsByCountryCode = (countryCode) => {
  if (!countryCode) {
    return [];
  }

  return City.getCitiesOfCountry(countryCode)
    .filter((city) => !city.name.includes("Obshtina"))
    .map((city) => ({
      value: city,
      label: city.name,
    }));
};

const validateForm = (formValues, hasDifferentAddress) => {
  const errList = [];
  if (!formValues.firstName) {
    errList.push("First name is required!");
  }

  if (!formValues.lastName) {
    errList.push("Last name is required!");
  }

  if (!formValues.country) {
    errList.push("Country is required!");
  }

  if (!formValues.city) {
    errList.push("City is required!");
  }

  if (!formValues.zip) {
    errList.push("Zip is required!");
  } else {
    if (!postcodeValidator(formValues.zip, formValues.countryCode)) {
      errList.push("Zip is invalid!");
    }
  }

  if (!formValues.phone) {
    errList.push("Phone number is required!");
  } else {
    if (!phone(formValues.phone, { country: formValues.countryCode }).isValid) {
      errList.push("Phone number is invalid!");
    }
  }

  if (!formValues.address) {
    errList.push("Address is required!");
  }

  if (hasDifferentAddress) {
    if (!formValues.shippingFirstName) {
      errList.push("Shipping first name is required!");
    }

    if (!formValues.shippingLastName) {
      errList.push("Shipping last name is required!");
    }

    if (!formValues.shippingPhone) {
      errList.push("Shipping phone number is required!");
    } else {
      if (
        !phone(formValues.shippingPhone, {
          country: formValues.shippingCountryCode,
        }).isValid
      ) {
        errList.push("Shipping phone number is invalid!");
      }
    }

    if (!formValues.shippingAddress) {
      errList.push("Shipping address is required!");
    }

    if (!formValues.shippingCountry) {
      errList.push("Shipping country is required!");
    }

    if (!formValues.shippingCity) {
      errList.push("Shipping city is required!");
    }

    if (!formValues.shippingZip) {
      errList.push("Shipping zip is required!");
    } else {
      if (
        !postcodeValidator(
          formValues.shippingZip,
          formValues.shippingCountryCode
        )
      ) {
        errList.push("Shipping zip is invalid!");
      }
    }
  }

  return errList;
};

const CheckoutForm = () => {
  const navigate = useNavigate();
  const [formValues, setFormValues] = useState({
    firstName: "",
    lastName: "",
    zip: "",
    phone: "",
    address: "",
    address2: "",
    city: "",
    country: "",
    shippingFirstName: "",
    shippingLastName: "",
    shippingZip: "",
    shippingPhone: "",
    shippingAddress: "",
    shippingAddress2: "",
    shippingCity: "",
    shippingCountry: "",
  });
  const [hasDifferentAddress, setHasDifferentAddress] = useState(false);
  const [cartProductsInfo, setCartProductsInfo] = useState(null);
  const { cartItems, clearCart } = useCartContext();

  useEffect(() => {
    getCartProductInfo(cartItems).then((response) =>
      setCartProductsInfo(response.data)
    );
  }, [cartItems]);

  const handleOnChange = useCallback(
    (name, value) =>
      setFormValues((prevState) => ({ ...prevState, [name]: value })),
    [setFormValues]
  );

  const handleOnCountryChange = useCallback(
    (name, value, name2, value2) =>
      setFormValues((prevState) => ({
        ...prevState,
        [name]: value,
        [name2]: value2,
      })),
    [setFormValues]
  );

  const countryOptions = useMemo(() => getCountryOptions(), []);
  const cityOptions = useMemo(
    () => getCitiesOptionsByCountryCode(formValues.countryCode),
    [formValues.countryCode]
  );

  const shippingCityOptions = useMemo(
    () => getCitiesOptionsByCountryCode(formValues.shippingCountryCode),
    [formValues.shippingCountryCode]
  );

  const onSubmit = useCallback(() => {
    const errors = validateForm(formValues, hasDifferentAddress);
    if (errors.length > 0) {
      toast.error(errors.join("\n"));
      return;
    }

    const orderedProducts = cartProductsInfo.products.map((product) => ({
      count: product.count,
      productId: product.id,
      priceAtPayment: product.price,
    }));
    toast.promise(
      createOrder({
        ...formValues,
        orderedProducts,
        hasShippingAddress: hasDifferentAddress,
      }),
      {
        loading: "Loading...",
        success: () => {
          clearCart();
          navigate("/checkout/success");

          return "Successfully Created!";
        },
        error: "Error while creating!",
      }
    );
  }, [formValues, cartProductsInfo, navigate, hasDifferentAddress, clearCart]);

  const handleChangeOnlyNumbers = useCallback(
    (name, value) => {
      if (value !== "" && !isOnlyNumbers(value)) {
        return;
      }
      handleOnChange(name, value);
    },
    [handleOnChange]
  );

  const handleChangeOnlyLetters = useCallback(
    (name, value) => {
      if (value !== "" && !isOnlyLetters(value)) {
        return;
      }
      handleOnChange(name, value);
    },
    [handleOnChange]
  );

  return (
    <>
      <div className="maincontainer">
        <div className="container">
          <div className="py-3 text-center">
            <StyledHeading>Checkout</StyledHeading>
          </div>
          <div className="row">
            {cartProductsInfo && (
              <CheckoutCartProducts cartProductsInfo={cartProductsInfo} />
            )}
            <CheckoutFormContainer className="col-md-8 order-md-1">
              <h4 className="mb-3">Billing address</h4>
              <form className="needs-validation">
                <div className="row">
                  <div className="col-md-6 mb-3">
                    <label htmlFor="firstName">First name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="firstName"
                      placeholder="Please enter first name..."
                      value={formValues.firstName}
                      onChange={(e) =>
                        handleChangeOnlyLetters("firstName", e.target.value)
                      }
                      required
                    />
                    <div className="invalid-feedback">
                      Valid first name is required.
                    </div>
                  </div>
                  <div className="col-md-6 mb-3">
                    <label htmlFor="lastName">Last name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="lastName"
                      placeholder="Please enter last name..."
                      value={formValues.lastName}
                      onChange={(e) =>
                        handleChangeOnlyLetters("lastName", e.target.value)
                      }
                      required
                    />
                    <div className="invalid-feedback">
                      Valid last name is required.
                    </div>
                  </div>
                </div>
                <div className="mb-3">
                  <label htmlFor="phone">Phone number</label>
                  <input
                    type="text"
                    className="form-control"
                    id="phone"
                    placeholder="Please enter a valid phone number"
                    onChange={(e) =>
                      handleChangeOnlyNumbers("phone", e.target.value)
                    }
                    value={formValues.phone}
                  />
                </div>
                <div className="row">
                  <div className="col-md-5 mb-3">
                    <label htmlFor="country">Country</label>
                    <Select
                      className="custom-select d-block w-100"
                      id="country"
                      options={countryOptions}
                      onChange={(newValue) =>
                        handleOnCountryChange(
                          "countryCode",
                          newValue.value.isoCode,
                          "country",
                          newValue.value.name
                        )
                      }
                      required
                    ></Select>
                    <div className="invalid-feedback">
                      Please select a valid country.
                    </div>
                  </div>
                  <div className="col-md-4 mb-3">
                    <label htmlFor="city">City</label>
                    <Select
                      className="custom-select d-block w-100"
                      id="city"
                      required
                      options={cityOptions}
                      onChange={(newValue) =>
                        handleOnChange("city", newValue.value.name)
                      }
                    ></Select>
                    <div className="invalid-feedback">
                      Please provide a valid state.
                    </div>
                  </div>
                  <div className="col-md-3 mb-3">
                    <label htmlFor="zip">Zip</label>
                    <input
                      type="text"
                      className="form-control"
                      id="zip"
                      placeholder=""
                      onChange={(e) =>
                        handleChangeOnlyNumbers("zip", e.target.value)
                      }
                      disabled={!formValues.countryCode}
                      value={formValues.zip}
                      required
                    />
                    <div className="invalid-feedback">Zip code required.</div>
                  </div>
                </div>
                <div className="mb-3">
                  <label htmlFor="address">Address</label>
                  <input
                    type="text"
                    className="form-control"
                    id="address"
                    placeholder="1234 Main St"
                    onChange={(e) => handleOnChange("address", e.target.value)}
                    value={formValues.address}
                    required
                  />
                  <div className="invalid-feedback">
                    Please enter your address.
                  </div>
                </div>
                <div className="mb-3">
                  <label htmlFor="address2">Address 2 </label>
                  <input
                    type="text"
                    className="form-control"
                    id="address2"
                    placeholder="Apartment or suite"
                    onChange={(e) => handleOnChange("address2", e.target.value)}
                    value={formValues.address2}
                  />
                </div>
                <hr className="mb-4" />
                <RadioButton className="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    id="same-address"
                    onClick={() => setHasDifferentAddress(!hasDifferentAddress)}
                  />
                  <label
                    className="custom-control-label"
                    htmlFor="same-address"
                  >
                    Shipping address is different than my billing address
                  </label>
                </RadioButton>

                {hasDifferentAddress && (
                  <>
                    <h4 className="mb-3 mt-3">Shipping address</h4>
                    <div className="row">
                      <div className="col-md-6 mb-3">
                        <label htmlFor="shippingFirstName">First name</label>
                        <input
                          type="text"
                          className="form-control"
                          id="shippingFirstName"
                          placeholder="Please enter first name..."
                          value={formValues.shippingFirstName}
                          onChange={(e) =>
                            handleChangeOnlyLetters(
                              "shippingFirstName",
                              e.target.value
                            )
                          }
                          required
                        />
                        <div className="invalid-feedback">
                          Valid first name is required.
                        </div>
                      </div>
                      <div className="col-md-6 mb-3">
                        <label htmlFor="shippingLastName">Last name</label>
                        <input
                          type="text"
                          className="form-control"
                          id="shippingLastName"
                          placeholder="Please enter last name..."
                          value={formValues.shippingLastName}
                          onChange={(e) =>
                            handleChangeOnlyLetters(
                              "shippingLastName",
                              e.target.value
                            )
                          }
                          required
                        />
                        <div className="invalid-feedback">
                          Valid last name is required.
                        </div>
                      </div>
                    </div>
                    <div className="mb-3">
                      <label htmlFor="shippingPhone">Phone number</label>
                      <input
                        type="text"
                        className="form-control"
                        id="shippingPhone"
                        placeholder="Please enter a valid phone number"
                        onChange={(e) =>
                          handleChangeOnlyNumbers(
                            "shippingPhone",
                            e.target.value
                          )
                        }
                        value={formValues.shippingPhone}
                      />
                    </div>
                    <div className="row">
                      <div className="col-md-5 mb-3">
                        <label htmlFor="country">Country</label>
                        <Select
                          className="custom-select d-block w-100"
                          id="country"
                          options={countryOptions}
                          onChange={(newValue) =>
                            handleOnCountryChange(
                              "shippingCountryCode",
                              newValue.value.isoCode,
                              "shippingCountry",
                              newValue.value.name
                            )
                          }
                          required
                        ></Select>
                        <div className="invalid-feedback">
                          Please select a valid country.
                        </div>
                      </div>
                      <div className="col-md-4 mb-3">
                        <label htmlFor="city">City</label>
                        <Select
                          className="custom-select d-block w-100"
                          id="city"
                          required
                          options={shippingCityOptions}
                          onChange={(newValue) =>
                            handleOnChange("shippingCity", newValue.value.name)
                          }
                        ></Select>
                        <div className="invalid-feedback">
                          Please provide a valid state.
                        </div>
                      </div>
                      <div className="col-md-3 mb-3">
                        <label htmlFor="zip">Zip</label>
                        <input
                          type="text"
                          className="form-control"
                          id="zip"
                          placeholder=""
                          onChange={(e) =>
                            handleChangeOnlyNumbers(
                              "shippingZip",
                              e.target.value
                            )
                          }
                          disabled={!formValues.shippingCountryCode}
                          value={formValues.shippingZip}
                          required
                        />
                        <div className="invalid-feedback">
                          Zip code required.
                        </div>
                      </div>
                    </div>
                    <div className="mb-3">
                      <label htmlFor="shippingAddress">Address</label>
                      <input
                        type="text"
                        className="form-control"
                        id="shippingAddress"
                        placeholder="1234 Main St"
                        onChange={(e) =>
                          handleOnChange("shippingAddress", e.target.value)
                        }
                        value={formValues.shippingAddress}
                        required
                      />
                      <div className="invalid-feedback">
                        Please enter your address.
                      </div>
                    </div>
                    <div className="mb-3">
                      <label htmlFor="shippingAddress2">Address 2</label>
                      <input
                        type="text"
                        className="form-control"
                        id="shippingAddress2"
                        placeholder="Apartment or suite"
                        onChange={(e) =>
                          handleOnChange("shippingAddress2", e.target.value)
                        }
                        value={formValues.shippingAddress2}
                      />
                    </div>
                  </>
                )}

                <hr className="mb-4" />
                <h4 className="mb-3">Payment Options</h4>
                <div className="d-block my-3">
                  <RadioButton className="custom-control custom-radio">
                    <input
                      id="debit"
                      name="paymentMethod"
                      type="radio"
                      className="custom-control-input"
                      checked="checked"
                      readOnly
                    />
                    <label className="custom-control-label" htmlFor="debit">
                      On Delivery
                    </label>
                  </RadioButton>
                </div>
                <hr className="mb-2" />
                <Form.Group className="mb-3">
                  <Form.Label>Delivery Comment</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={3}
                    placeholder="Delivery comment..."
                    onChange={(e) =>
                      handleOnChange("deliveryComment", e.target.value)
                    }
                    value={formValues.deliveryComment}
                  />
                </Form.Group>

                <NormalButton
                  className="btn-lg btn-block"
                  type="button"
                  onClick={onSubmit}
                >
                  Make an order
                </NormalButton>
              </form>
            </CheckoutFormContainer>
          </div>
        </div>
      </div>
    </>
  );
};

export default CheckoutForm;

const RadioButton = styled.div`
  display: flex;
  align-items: center;
  gap: 0.3rem;
  margin-bottom: 0.5rem;

  label {
    margin: 0;
  }
`;

const CheckoutFormContainer = styled.div`
  animation: 2s ${slideAnimation};
`;
