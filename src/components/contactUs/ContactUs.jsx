import { useCallback, useState } from "react";
import { FloatingLabel, Form } from "react-bootstrap";
import styled from "styled-components";
import { contactUsEmail } from "../../apis/emails";
import {
  breakpointsMedia,
  companyEmail,
  phoneNumber,
  pulseAnimation,
  zoomInAnimation,
} from "../../constants";

import toast from "react-hot-toast";
import {
  ResponsiveButton,
  StyledHeading,
} from "../commonComponents/CommonStyledComponents";

const validate = ({ name, email, subject, message }) => {
  const errorMessages = [];

  if (!name.trim()) {
    errorMessages.push("Name is mandatory!");
  }
  if (!email.trim()) {
    errorMessages.push("Email is mandatory!");
  }
  if (!subject.trim()) {
    errorMessages.push("Subject is mandatory!");
  }

  const messageBody = message.trim();
  if (!messageBody) {
    errorMessages.push("Message is mandatory!");
  }
  if (messageBody && messageBody.length < 25) {
    errorMessages.push("Message should be at least 25 characters!");
  }

  return errorMessages;
};

const ContactUs = () => {
  const [mailData, setMailData] = useState({
    name: "",
    email: "",
    subject: "",
    message: "",
  });

  const onSubmit = useCallback(() => {
    const errorMessages = validate(mailData);
    if (errorMessages.length > 0) {
      toast.error(errorMessages.join("\n"));
      return;
    }

    toast.promise(contactUsEmail(mailData), {
      loading: "Loading...",
      success: "Successfully sent!",
      error: "Error while sending!",
    });
  }, [mailData]);

  const handleOnChange = (e) => {
    toast.dismiss();
    setMailData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  return (
    <Section className="mb-4">
      <StyledHeading className="h1-responsive font-weight-bold text-center my-4">
        Contact us
      </StyledHeading>

      <p className="text-center w-responsive mx-auto mb-5">
        Do you have any questions? Please do not hesitate to contact us
        directly. Our team will come back to you within a matter of hours to
        help you.
      </p>

      <div className="row">
        <div className="col-md-9 mb-md-0 mb-5">
          <form
            id="contact-form"
            name="contact-form"
            action="mail.php"
            method="POST"
          >
            <div className="row mb-2 p-3">
              <div className="col-md-6 mb-5 mb-md-0">
                <div className="md-form mb-0">
                  <FloatingLabel label="Your name">
                    <Form.Control
                      type="text"
                      onChange={handleOnChange}
                      name="name"
                      placeholder="Your name"
                    />
                  </FloatingLabel>
                </div>
              </div>
              <div className="col-md-6">
                <div className="md-form mb-0">
                  <FloatingLabel label="Email address">
                    <Form.Control
                      type="email"
                      onChange={handleOnChange}
                      name="email"
                      placeholder="Email address"
                    />
                  </FloatingLabel>
                </div>
              </div>
            </div>
            <div className="row mb-2 p-3">
              <div className="col-md-12">
                <div className="md-form mb-0">
                  <FloatingLabel label="Subject">
                    <Form.Control
                      type="text"
                      onChange={handleOnChange}
                      name="subject"
                      placeholder="Subject"
                    />
                  </FloatingLabel>
                </div>
              </div>
            </div>
            <div className="row mb-2 p-3">
              <div className="col-md-12">
                <div className="md-form">
                  <FloatingLabel
                    controlId="floatingTextarea2"
                    label="Your message"
                  >
                    <Form.Control
                      as="textarea"
                      style={{ height: "5rem" }}
                      onChange={handleOnChange}
                      name="message"
                      placeholder="Your message"
                    />
                  </FloatingLabel>
                </div>
              </div>
            </div>
          </form>

          <div className="text-center text-md-left p-3">
            <SendButton className="btn btn-primary" onClick={onSubmit}>
              Send
            </SendButton>
          </div>
          <div className="status"></div>
        </div>
        <CompanyInformation className="col-md-3 text-center">
          <ul className="list-unstyled mb-0">
            <li>
              <Icon className="fa fa-map-marker fa-2x"></Icon>
              <p>Plovdiv, Bulgaria</p>
            </li>

            <li>
              <Icon className="fa fa-phone mt-4 fa-2x"></Icon>
              <p>{phoneNumber}</p>
            </li>

            <li>
              <Icon className="fa fa-envelope mt-4 fa-2x"></Icon>
              <p>{companyEmail}</p>
            </li>
          </ul>
        </CompanyInformation>
      </div>
    </Section>
  );
};

export default ContactUs;

const CompanyInformation = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const SendButton = styled(ResponsiveButton)`
  width: 100%;

  ${breakpointsMedia.tablet} {
    width: 25%;
  }
`;

const Section = styled.section`
  animation: 1s ${zoomInAnimation};
`;

const Icon = styled.i`
  animation: 1.5s ${pulseAnimation};
  animation-iteration-count: infinite;
  color: #662bb7;
`;
