export const StatusEnum = {
  CUSTOM: "Custom",
  RECEIVED: "Received",
  IN_PROGRESS: "In progress",
  DELIVERED: "Delivered",
  CANCELLED: "Cancelled",
};

export const getAllStatusesAsSelectOptions = () =>
  Object.keys(StatusEnum).map((status) => ({
    label: StatusEnum[status],
    value: status,
  }));
