import axios from "axios";
import { orderServiceUrl } from "../constants";
import { StatusEnum } from "../enums/status";

export const orderSortingOptions = [
  {
    value: { sortColumn: "createdOn", sortDirection: "asc" },
    label: "Sort by created on - Ascending",
  },
  {
    value: { sortColumn: "createdOn", sortDirection: "desc" },
    label: "Sort by created on - Descending",
  },
  {
    value: { sortColumn: "updatedOn", sortDirection: "asc" },
    label: "Sort by updated on - Ascending",
  },
  {
    value: { sortColumn: "updatedOn", sortDirection: "desc" },
    label: "Sort by updated on - Descending",
  },
];

export const createOrder = (orderData) =>
  axios.post(`${orderServiceUrl}/orders`, orderData);

export const getAllOrders = (
  filters = { sorting: orderSortingOptions[0].value, page: 0, pageSize: 10 }
) =>
  axios.get(`${orderServiceUrl}/orders`, {
    params: {
      status: filters.status,
      pageSize: filters.pageSize,
      page: filters.page,
      sortColumn:
        filters?.sorting?.sortColumn ?? orderSortingOptions[0].value.sortColumn,
      sortDirection:
        filters?.sorting?.sortDirection ??
        orderSortingOptions[0].value.sortDirection,
      orderNumber: filters.orderNumber,
    },
  });

export const cancelOrder = (orderId) =>
  axios.put(
    `${orderServiceUrl}/orders/${orderId}?status=${StatusEnum.CANCELLED}`
  );

export const updateOrderStatus = (orderId, status) =>
  axios.put(`${orderServiceUrl}/orders/${orderId}?status=${status}`);
