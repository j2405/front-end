import axios from "axios";
import { productServiceUrl } from "../constants";

export const createMaterial = ({ name, color }) =>
  axios.post(`${productServiceUrl}/materials`, {
    name,
    color,
  });

export const getAllMaterials = () =>
  axios.get(`${productServiceUrl}/materials`);
