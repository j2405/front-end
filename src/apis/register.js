import axios from "axios";
import { userServiceUrl } from "../constants";
import base64 from "base-64";

export const register = (registrationData) => {
  const options = {
    method: "POST",
    url: `${userServiceUrl}/register`,
    data: {
      ...registrationData,
      password: base64.encode(registrationData.password),
      passwordConfirm: base64.encode(registrationData.passwordConfirm),
    },
  };
  return axios(options);
};
