import axios from "axios";
import { mailServiceUrl } from "../constants";

export const contactUsEmail = ({ name, message, email, subject }) =>
  axios.post(`${mailServiceUrl}/api/mail/contact`, {
    subject,
    name,
    message,
    fromEmail: email,
  });

export const customOrderEmail = ({ message, files = [] }) => {
  var bodyFormData = new FormData();
  for (const element of files) {
    bodyFormData.append("files", element);
  }

  return axios({
    method: "post",
    url: `${mailServiceUrl}/api/mail/custom?message=${message}`,
    data: bodyFormData,
    headers: { "Content-Type": "multipart/form-data" },
  });
};
