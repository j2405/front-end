import axios from "axios";
import { productServiceUrl } from "../constants";

export const createProducts = ({ files, ...productData }) => {
  var bodyFormData = new FormData();
  Object.keys(productData).forEach((key) =>
    bodyFormData.append(key, productData[key])
  );
  for (const element of files) {
    bodyFormData.append("files", element);
  }

  return axios({
    method: "post",
    url: `${productServiceUrl}/products`,
    data: bodyFormData,
    headers: { "Content-Type": "multipart/form-data" },
  });
};

export const getAllProducts = (
  {
    pageSize = 12,
    page = 0,
    sorting = { sortColumn: "name", sortDirection: "asc" },
    categoryId,
    priceRange,
    name,
    gem,
    material,
  } = {
    pageSize: 12,
    page: 0,
    sorting: { sortColumn: "name", sortDirection: "asc" },
  }
) =>
  axios.get(`${productServiceUrl}/products`, {
    params: {
      sortColumn: sorting.sortColumn,
      sortDirection: sorting.sortDirection,
      pageSize,
      page,
      categoryId,
      name,
      gem,
      material,
      minPrice: priceRange && priceRange.min,
      maxPrice: priceRange && priceRange.max,
    },
  });

export const getPriceRangeForProducts = (
  {
    pageSize = 12,
    page = 0,
    sorting = { sortColumn: "name", sortDirection: "asc" },
    categoryId,
    priceRange,
    name,
  } = {
    pageSize: 12,
    page: 0,
    sorting: { sortColumn: "name", sortDirection: "asc" },
  }
) =>
  axios.get(`${productServiceUrl}/products/price-range`, {
    params: {
      sortColumn: sorting.sortColumn,
      sortDirection: sorting.sortDirection,
      pageSize,
      page,
      categoryId,
      name,
      minPrice: priceRange && priceRange.min,
      maxPrice: priceRange && priceRange.max,
    },
  });

export const getProduct = (productId) =>
  axios.get(`${productServiceUrl}/products/${productId}`);

export const getCartProductInfo = (productIds) =>
  axios.get(
    `${productServiceUrl}/products/cart?productIds=${productIds.join(",")}`
  );
