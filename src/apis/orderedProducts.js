import axios from "axios";
import { orderServiceUrl } from "../constants";

export const getAllOrderedProducts = (orderId) =>
  axios.get(`${orderServiceUrl}/orders/${orderId}/products`);
