import axios from "axios";
import { productServiceUrl } from "../constants";

const categoriesEndpoint = `${productServiceUrl}/categories`;

export const createCategory = ({ name }) =>
  axios.post(categoriesEndpoint, {
    name,
  });

export const getAllCategories = () => axios.get(categoriesEndpoint);

export const getCategoryByName = (name) =>
  axios.get(`${categoriesEndpoint}/${name}`);
