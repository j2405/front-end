import axios from "axios";
import { userServiceUrl } from "../constants";
import base64 from "base-64";

export const login = (loginData) => {
  const options = {
    method: "POST",
    url: `${userServiceUrl}/login`,
    data: { ...loginData, password: base64.encode(loginData.password) },
  };
  return axios(options);
};
