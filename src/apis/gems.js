import axios from "axios";
import { productServiceUrl } from "../constants";

const gemsEndpoint = `${productServiceUrl}/gems`;

export const createGem = ({ name }) =>
  axios.post(gemsEndpoint, {
    name,
  });

export const getAllGems = () => axios.get(gemsEndpoint);
