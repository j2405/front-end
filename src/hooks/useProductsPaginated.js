import { useCallback, useEffect, useRef, useState } from "react";
import { getAllProducts } from "../apis/products";
import { useSearchParams } from "react-router-dom";
import _ from "lodash";
import { selectOptions } from "../components/products/ProductsPage";

export const useProductsPaginated = () => {
  const [page, setPage] = useState({ products: [], hasNext: false });
  const [loading, setLoading] = useState(true);
  const initialLoad = useRef(true);

  const [searchParams] = useSearchParams();
  const searchName = searchParams.get("name");
  const categoryIdQueryParam = searchParams.get("categoryId");
  const [options, setOptions] = useState({
    name: searchName,
    sorting: selectOptions[0].value,
    pageSize: 12,
    categoryId: categoryIdQueryParam ?? null,
    priceRange: null,
    gem: null,
    material: null,
    page: 0,
  });

  const previousPage = useRef(options.page);

  const handleOnChange = useCallback(
    (name, value) =>
      setOptions((prevState) => ({ ...prevState, page: 0, [name]: value })),
    []
  );

  const onLoadMoreCLick = useCallback(
    () =>
      setOptions((prevState) => ({ ...prevState, page: prevState.page + 1 })),
    []
  );

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const debouncedChangeHandler = useCallback(_.debounce(handleOnChange, 50), [
    handleOnChange,
  ]);

  useEffect(() => {
    if (initialLoad.current) {
      return;
    }
    handleOnChange("name", searchName);
  }, [searchName, handleOnChange]);

  useEffect(() => {
    if (initialLoad.current) {
      return;
    }
    handleOnChange("categoryId", categoryIdQueryParam);
  }, [categoryIdQueryParam, handleOnChange]);

  useEffect(() => {
    getAllProducts(options).then(({ data }) => {
      setPage((prevState) => ({
        ...data,
        products:
          previousPage.current !== options.page && options.page !== 0
            ? [...prevState.products, ...data.products]
            : data.products,
      }));
      previousPage.current = options.page;
      initialLoad.current = false;
      setLoading(false);
    });

    return () => setLoading(true);
  }, [options]);

  return {
    debouncedChangeHandler,
    onLoadMoreCLick,
    handleOnChange,
    page,
    loading,
    initialLoad: initialLoad.current,
    options,
    previousPage: previousPage.current,
  };
};
