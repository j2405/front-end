import _ from "lodash";
import { useCallback, useEffect, useRef, useState } from "react";
import { getAllOrders, orderSortingOptions } from "../apis/orders";

export const useOrdersPaginated = () => {
  const [page, setPage] = useState({ orders: [], hasNext: false });
  const [options, setOptions] = useState({
    sorting: orderSortingOptions[0],
    page: 0,
    status: null,
    pageSize: 10,
  });
  const [loading, setLoading] = useState(true);
  const previousPage = useRef(options.page);

  const handleOnChange = useCallback(
    (name, value) =>
      setOptions((prevState) => ({ ...prevState, page: 0, [name]: value })),
    [setOptions]
  );

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const debouncedChangeHandler = useCallback(_.debounce(handleOnChange, 400), [
    handleOnChange,
  ]);

  const onLoadMoreCLick = useCallback(
    () =>
      setOptions((prevState) => ({ ...prevState, page: prevState.page + 1 })),
    []
  );

  const fetchAndSetOrders = useCallback(
    () =>
      getAllOrders(options).then(({ data }) => {
        setPage((prevState) => ({
          ...data,
          orders:
            previousPage.current !== options.page && options.page !== 0
              ? [...prevState.orders, ...data.orders]
              : data.orders,
        }));
        previousPage.current = options.page;
        setLoading(false);
      }),
    [options]
  );

  useEffect(() => {
    fetchAndSetOrders();

    return () => setLoading(true);
  }, [fetchAndSetOrders]);

  return {
    hasNext: page.hasNext,
    orders: page.orders,
    handleOnChange,
    fetchAndSetOrders,
    options,
    loading,
    onLoadMoreCLick,
    debouncedChangeHandler,
  };
};
