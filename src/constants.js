import { slideInLeft } from "react-animations";
import { slideInUp, pulse } from "react-animations";
import { zoomIn } from "react-animations";
import { fadeIn } from "react-animations";
import { keyframes } from "styled-components";

export const userServiceUrl = "http://localhost:8765/user-service";
export const mailServiceUrl = "http://localhost:8765/mailing-service";
export const productServiceUrl = "http://localhost:8765/product-service";
export const orderServiceUrl = "http://localhost:8765/order-service";
export const instagramName = "the_absolute.creations";
export const facebookName = "absl.creations";
export const phoneNumber = "+359 234 567 89";
export const companyEmail = "absolute-creations@gmail.com";

const breakpoints = {
  mobile: 375,
  tablet: 768,
  laptop: 1440,
};
export const breakpointsMedia = {
  mobile: `@media screen and (min-width: ${breakpoints.mobile}px)`,
  tablet: `@media screen and (min-width: ${breakpoints.tablet}px)`,
  laptop: `@media screen and (min-width: ${breakpoints.laptop}px)`,
};

export const isMobile = window.screen.width < breakpoints.tablet;
export const isTablet =
  window.screen.width >= breakpoints.tablet &&
  window.screen.width < breakpoints.laptop;
export const isDesktop = window.screen.width >= breakpoints.laptop;

export const fadeInAnimation = keyframes`${fadeIn}`;
export const slideAnimation = keyframes`${slideInLeft}`;
export const zoomInAnimation = keyframes`${zoomIn}`;
export const slideInUpAnimation = keyframes`${slideInUp}`;
export const pulseAnimation = keyframes`${pulse}`;

const ONLY_LETTERS_REGEX = /^[A-Za-z]+$/;
export const isOnlyLetters = (text) => ONLY_LETTERS_REGEX.test(text);

const ONLY_NUMBERS_REGEX = /^\d+$/;
export const isOnlyNumbers = (text) => ONLY_NUMBERS_REGEX.test(text);
